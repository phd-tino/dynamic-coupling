""" Brief : Module containing usefull functions and Finite Volume solvers for kinetic, fluid and hybrid method
    Author : Laidin Tino, Université de Nantes
    Date : July 2021
"""
from FV_tools_Efield import *

##################################################################
#                      NON COUPLED SOLVERS                       #
##################################################################
def CenteredFluxPos(F):
    """ Brief: Centered flux with periodic BC
    
    Input :
    - F:   F at t^n                         (meshgrid form)
    - V:   Cell centers in velocities       (meshgrid form)
    - DV:  Cell lengths in velocity (meshgrid form)
    
    Output :
    - F:     F   at t^n+1                   (meshgrid form)
    """
    
    Nv = F.shape[0]
    Nx = F.shape[1]
    
    # Ghost cells
    lg  = -1
    rg  = 0
    
    # Memory allocation
    FluxPos   = np.zeros((Nv,Nx+1))

    # Centered flux
    FluxPos[:,0]    = 0.5*(F[:,lg]+F[:,0])
    FluxPos[:,1:-1] = 0.5*(F[:,:-1]+F[:,1:])
    FluxPos[:,-1]   = 0.5*(F[:,-1]+F[:,rg])
    
    return FluxPos

def CenteredFluxVel(F):
    """ Brief: Centered flux in velocity with homegeneous Neumann BC
    
    Input :
    - F:   F at t^n                         (meshgrid form)
    
    Output :
    - FluxVel: Centered flux in Velocity    (meshgrid form)
    """
    
    Nv = F.shape[0]
    Nx = F.shape[1]
    
    # Ghost cells
    lg  = -1
    rg  = 0
    
    # Memory allocation
    FluxVel   = np.zeros((Nv+1,Nx))

    # Centered flux
    FluxVel[0,:]    = 0 # Homegeneous Neumann BC
    FluxVel[1:-1,:] = 0.5*(F[:-1,:]+F[1:,:])
    FluxVel[-1,:]   = 0 # Homegeneous Neumann BC
    
    return FluxVel

def solve_Kinetic(F, E, MM, invDX, V, DV, invDV, dt, m2, inveps):
    """ Brief: Solves the Vlasov equation on the whole domain
    
    Input :
    - F:      F at t^n                         (meshgrid form)
    - E:      Exterior electrical field        (meshgrid form)
    - MM:     Maxwellian                       (meshgrid form)
    - invDX:  One over cell length in position (meshgrid form)
    - V:      Cell centers in velocities       (meshgrid form)
    - invDV:  One over cell length in velocity (meshgrid form)
    - dt:     Time step
    - inveps: Knudsen number
    
    Output :
    - F:     F   at t^n+1                      (meshgrid form)
    """
    
    Nv = F.shape[0]
    Nx = F.shape[1]
    
    # Memory allocation
    FluxPos = CenteredFluxPos(F)
    FluxVel = CenteredFluxVel(F)
    # Transport in space
    Fn = F - dt*inveps*invDX*(FluxPos[:,1:]-FluxPos[:,:-1])*V 
    # Transport in velocity
    Fn += - dt*inveps*invDV*(FluxVel[1:,:] - FluxVel[:-1,:])*E
    # Stiff term
    Fn += dt*inveps*inveps*(np.tile(np.transpose(np.sum(DV*F, axis=0)),(Nv,1))*MM - F)
    
    return Fn

def solve_Fluid(rho, E, alpha, invdx, dt):
    """ Brief: Solves the limit scheme on the whole domain
    
     Input :
     - rho:   rho at t^n
     - E:      Exterior electrical field
     - alpha: Diffusion coefficient
     - invdx: Inverse of the space discretization
     - dt:    Time step
    
     Output :
     - rhon:  Updated fluid limit
    """
    
    Nx = rho.shape[0]
    # Ghost cells
    llg = -2
    lg  = -1
    rg  = 0
    rrg = 1
    
    # Memory allocation
    diffu = np.zeros(Nx)
    drift = np.zeros(Nx)
    
    diffu[0]    = invdx[1]*(rho[2]-rho[0])         - invdx[lg]*(rho[0]-rho[llg])
    diffu[1]    = invdx[2]*(rho[3]-rho[1])         - invdx[0]*(rho[1]-rho[lg])
    diffu[2:-2] = invdx[3:-1]*(rho[4:]-rho[2:-2])  - invdx[1:-3]*(rho[2:-2]-rho[:-4])
    diffu[-2]   = invdx[-1]*(rho[rg]-rho[-2])      - invdx[-3]*(rho[-2]-rho[-4])
    diffu[-1]   = invdx[rg]*(rho[rrg]-rho[-1])     - invdx[-2]*(rho[-1]-rho[-3])
    
    drift[0]    = E[1]*rho[1]   - E[lg]*rho[lg]
    drift[1:-1] = E[2:]*rho[2:] - E[:-2]*rho[:-2]
    drift[-1]   = E[rg]*rho[rg] - E[-2]*rho[-2]

    rhon = rho + dt*alpha*0.25*invdx*diffu - dt*0.5*invdx*drift
    return rhon

##################################################################
#                      DYNAMIC COUPLING                          #
################################################################## 

#  --------------- COUPLING TOOLS --------------------
def indicators(rho_kin, rho_fluid, E, dxE, invdx):
    
    llg = -2
    lg  = -1
    rg  = 0
    rrg = 1
    
    indKinetic = np.zeros(invdx.shape[0])
    indFluid   = np.zeros(invdx.shape[0])
    
    H    = np.zeros(invdx.shape[0])
    H[0]    = (rho_kin[1]  - rho_kin[lg])*invdx[0]*0.5 - E[0]*rho_kin[0]
    H[1:-1] = (rho_kin[2:] - rho_kin[:-2])*invdx[1:-1]*0.5  - E[1:-1]*rho_kin[1:-1]
    H[-1]   = (rho_kin[rg] - rho_kin[-2])*invdx[-1]*0.5 - E[-1]*rho_kin[-1]
    dxH  = Calc_dx(H, invdx)
    dxxH = Calc_dx(dxH,invdx)
    indKinetic = E*dxH + 2.0*H*dxE - dxxH
    
    H[0]    = (rho_fluid[1]  - rho_fluid[lg])*invdx[0]*0.5 - E[0]*rho_fluid[0]
    H[1:-1] = (rho_fluid[2:] - rho_fluid[:-2])*invdx[1:-1]*0.5  - E[1:-1]*rho_fluid[1:-1]
    H[-1]   = (rho_fluid[rg] - rho_fluid[-2])*invdx[-1]*0.5 - E[-1]*rho_fluid[-1]
    dxH  = Calc_dx(H, invdx)
    dxxH = Calc_dx(dxH,invdx)
    indFluid = E*dxH + 2.0*H*dxE - dxxH
    
    return (indKinetic, indFluid)

def get_indicator(H, E, dxE, invdx): 
    dxH  = Calc_dx(H, invdx)
    dxxH = Calc_dx(dxH,invdx)
    indKinetic = E*dxH + 2.0*H*dxE - dxxH
    return (indKinetic)

def updateState(indicator, L1G, isKinetic, eta, delta, epsilon):
    """ Brief: Computes the new kinetic and fluid cells of the domain based on their current state
    
    Input:
    - indicator
    - L1G:       L1 norm of  G at t^n : ||g(t^n, x_i+.5, . )||_L1
    - isKinetic: Array of boolean, each element corresponds to the state of a cell
    - eta:       Threshold determining the change from fluid to kinetic
    - delta:     Threshold determining the change from kinetic to fluid
    - eps:       Knudsen number
    - invdx:     
    
    Output:
    - new_state: Boolean list of updated cells' state
    """
    
    Nx = indicator.shape[0]
    toFluid   = np.full(Nx,True, dtype=bool)
    toKinetic = np.full(Nx,True, dtype=bool)
    
    toFluid = toFluid & (L1G  < delta)
    toFluid = toFluid & (indicator <= eta)   # Stays fluid criterion
    
    toKinetic = indicator>eta
    
    # If Kinetic and tofluid, flip ; If Fluid and toKinetic, flip
    newState = np.where( (isKinetic & toFluid) | ( ~isKinetic & toKinetic), ~isKinetic, isKinetic)
    
    # Storing regions with two ghost cells on each side
    regions = [[-2,-1,0]] # left ghostcell by periodicity and first cell
    prev = newState[0]
    count = 0
    for i in range(1,Nx-1):
        curr = newState[i]
        if curr == prev:
            regions[count].append(i)
        else:
            if i != Nx-2:
                regions[count].extend([i,i+1])  # Right ghostcells of current region
            else:
                regions[count].extend([i+1,-1])   # Right ghostcells of current region
            count += 1                            # Pass to the next region
            regions.append([i-2,i-1, i])          # First cell of next region and left ghostcells
        prev = curr
        
    # Dealing with the last cell 
    curr = newState[Nx-1]
    if  curr == prev:
        regions[count].extend([Nx-1,0,1]) # last cell and right ghostcells by periodicity
    else:
        regions[count].extend([Nx-1, 0]) # Right ghostcells of the second to last region
        regions.append([Nx-3, Nx-2, Nx-1, 0, 1]) # Last region is the single last cell
    
    return (newState, regions)

def CenteredFluxPos_Coupling(region, F):
    """ Brief: Centered flux in position in a coupling setting
    
    Input :
    - region: Kinetic subdomain
    - F:      F at t^n                        (meshgrid form)
    
    Output :
    - FluxPos:  centered flux in position     (meshgrid form)
    """
    
    Nv = F.shape[0]
    Nx = len(region)-4
    
    # Specific cells
    lg    = region[1]
    first = region[2]
    last  = region[-3]
    rg    = region[-2]
    
    # Slices
    i   = np.s_[:,first:last]   # Local slice i
    ip1 = np.s_[:,first+1:last+1] # Local slice 1+1
    
    # Memory allocation
    FluxPos   = np.zeros((Nv,Nx+1))
    
    # Centered flux
    FluxPos[:,0]    = 0.5*(F[:,lg]+F[:,first])
    FluxPos[:,1:-1] = 0.5*(F[i]+F[ip1])
    FluxPos[:,-1]   = 0.5*(F[:,last]+F[:,rg])
    
    return FluxPos

def CenteredFluxVel_Coupling(region, F):
    """ Brief: Centered flux in velocity in a coupling setting
    
    Input :
    - region: Kinetic subdomain
    - F:      F at t^n                        (meshgrid form)
    
    Output :
    - FluxVel:  centered flux in velocity     (meshgrid form)
    """
    
    Nv = F.shape[0]
    Nx = len(region)-4
    
    # Specific cells
    lg    = region[1]
    first = region[2]
    last  = region[-3]
    rg    = region[-2]
    
    # Memory allocation
    FluxVel   = np.zeros((Nv+1,Nx))

    # Centered flux
    FluxVel[0,:]    = 0 # Homegeneous Neumann BC
    FluxVel[1:-1,:] = 0.5*(F[:-1,first:last+1]+F[1:,first:last+1])
    FluxVel[-1,:]   = 0 # Homegeneous Neumann BC
    
    return FluxVel

def ProjToFluid(Fn_loc, DV_loc):
    """"Brief: Project the kinetic solution onto the fluid scale
    
     Input :
     - Fn_loc: Local kinetic solution
     - DV_loc: Local velocity steps

     Output :
     - proj : projected solution
    """
    
   
    proj = np.sum(Fn_loc*DV_loc, axis=0)
    
     # Add trapezoidal rule ?
        
    return proj

def liftToKin(region, rho, H, dxH, E, V, MM, invDX, eps, m2, order):
    """"Brief: Lifts the density rho to the distribution function f using the Chapmann Enskog expansion
    
     Input :
     - region: Fluid subdomain on which to lift the fluid solution
     - rho:    rho at t^n
     - V:      Cell centers in velocities       (meshgrid form)
     - MM:     Maxwellian                       (meshgrid form)
     - invDX:  One over cell length in position (meshgrid form)
     - eps:    Knudsen number
     - m2:     2nd discrete moment of the maxwellian 
     - order:  Order considered in the C-E expansion

     Output :
     - F_lifted : lifted distribution function in the region
    """
    
    Nv = invDX.shape[0]
    Nx = len(region)-4
    
    # Specific cells
    llg   = region[0]
    lg    = region[1]
    first = region[2]
    last  = region[-3]
    rg    = region[-2]
    rrg   = region[-1]
    
    # Slices
    s   = np.s_[:, first:last+1] # Local slice
    
    F_lifted = np.tile(rho[first:last+1].T,(Nv,1))*MM[s]
    if order==1:   
        # g^{(1)}
        F_lifted += -eps*V[s]*MM[s]*np.tile(H[first:last+1].T,(Nv,1))
    elif order==2:
        dxrho=Calc_dx(rho,invDX[0,:])
        # g^{(1)}
        F_lifted += -eps*invDX[s]*V[s]*MM[s]*np.tile(dxrho[first:last+1].T,(Nv,1))
        # g^{(2)}
        F_lifted += eps*eps*MM[s]*(-m2+V[s]*V[s])*np.tile(dxH[first:last+1].T,(Nv,1))
        F_lifted += eps*eps*MM[s]*E[s]*(np.tile(dxrho[first:last+1].T,(Nv,1))
                                *(V[s]*V[s]-1)+E[s]*np.tile(rho[first:last+1].T,(Nv,1))*(V[s]*V[s]*V[s]-3.0*V[s]))
#     elif order == 3:
#         dxrho=Calc_dx(rho,invdx)
#         # g^{(1)}
#         F_lifted += -eps*invDX[s]*V[s]*MM[s]*np.tile(dxrho[first:last+1].T,(Nv,1))
#         # g^{(2)}
#         F_lifted += eps*eps*MM[s]*(-m2+V[s]*V[s])*np.tile(dxH[first:last+1].T,(Nv,1))
#         F_lifted += eps*eps*MM[s]*E[s](np.tile(dxrho[first:last+1].T,(Nv,1))
#                                 *(V[s]*V[s]-1)+E[s]*np.tile(rho[first:last+1].T,(Nv,1))*(V[s]*V[s]*V[s]-3.0*V[s]))
    
    return F_lifted

#  ------------ ADAPTED LOCAL SOLVERS ----------------

def solve_Kinetic_Coupling(region, F, E, isKinetic, MM, invDX, V, DV, invDV, dt, m2, eps, inveps):
    """ Brief: Solves the Vlasov equation in a coupling setting
    
    Input :
    - region: Kinetic subdomain
    - F:      F at t^n                         (meshgrid form)
    - E:      Exterior electrical fieald       (meshgrid form)
    - rho:    rho at t^n
    - MM:     Maxwellian                       (meshgrid form)
    - invDX:  One over cell length in position (meshgrid form)
    - V:      Cell centers in velocities       (meshgrid form)
    - invDV:  One over cell length in velocity (meshgrid form)
    - dt:     Time step
    - inveps: Knudsen number
    
    Output :
    - F:     F   at t^n+1                      (meshgrid form)
    """
    
    Nv = F.shape[0]
    Nx = len(region)-4
    # Slice
    s   = np.s_[:, region[2]: region[-3]+1]  # Local slice
    # Numerical flux
    FluxPos = CenteredFluxPos_Coupling(region, F)
    FluxVel = CenteredFluxVel_Coupling(region, F)
    # Transport in position
    Fn = F[s] - dt*inveps*invDX[s]*(FluxPos[:,1:]-FluxPos[:,:-1])*V[s]
    # Transport in velocity
    Fn += - dt*inveps*invDV[s]*(FluxVel[1:,:] - FluxVel[:-1,:])*E[s]
    # Stiff term
    Fn += dt*inveps*inveps*(np.tile(np.sum(DV[s]*F[s], axis=0).T,(Nv,1))*MM[s] - F[s])

    return Fn

def solve_Fluid_Coupling(region, rho, dxH, dt):
    """ Brief: Solves the drift-diffusion limit with a diffusion coefficient alpha (large stencil)
    
     Input :
     - region: 
     - rho:    rho at t^n
     - dxH:
     - dt:     Time step
    
     Output :
     - fluid_new: updated heat
    """

    return rho[region[2]:region[-3]+1] + dt*dxH[region[2]:region[-3]+1]

#  ------------ GLOBAL COUPLED SOLVER ----------------

def solve_Coupled(rho, F, E, dxE, H, dxH, isKinetic, regions, eta, delta,
                  MM, invDX, V, DV, invDV, dt, m2, m4, epsilon, inveps, Coupling, order, i):
    """ Brief: Solves the Vlasov equation at time t using a dynamic coupling method
    
    Input :
    - rho:       rho at t^n 
    - F:         F   at t^n                       (meshgrid form)
    - E:         Exterior electrical field
    - H:
    - dxH:       Drift-diffusion term
    - isKinetic: Array of the current state of the cells (Boolean)
    - regions:   List of list containing a given region indexes
    - MM:        Maxwellian                       (meshgrid form)
    - eta:       Threshold determining the change from fluid to kinetic
    - delta:     Threshold determining the change from kinetic to fluid
    - invDX:     One over cell length in position (meshgrid form)
    - V:         Cell centers in velocities       (meshgrid form)
    - DV:        Cell length in velocity          (meshgrid form)
    - m2,m4:     2nd and 4th Maxwellian moments
    - eps:       Knudsen number
    - inveps:    Inverse of the Knudsen number
    - Coupling:  Boolean that enables dynamic coupling
    
    Output :
    - Fn:         F at t^n+1
    - rhon:       rho at t^n+1
    - isKineticn: Updated state
    - regionsn:   Updated regions
    """
    
    Nv = F.shape[0]
    Nx = F.shape[1]
  
    # Specific cells
    llg = -2
    lg  = -1
    rg  = 0
    rrg = 1

    # Memory allocation
    rhon  = np.zeros(Nx)
    Fn    = np.zeros((Nv,Nx))
    H     = np.zeros(Nx)
    dxH   = np.zeros(Nx)
       
    # Update micro macro unknowns
    for region in regions: # Solve per inner regions with appropriate boundary conditions
        # Slices
        s_phase = np.s_[:, region[2]:region[-3]+1]
        s_space = np.s_[region[2]:region[-3]+1]
        im1 = np.s_[region[2]:region[-3]-1]   # Local slice i-1
        ip1 = np.s_[region[2]+2:region[-3]+1] # Local slice 1+1
        
        if isKinetic[region[2]]: # Use kinetic model
            Fn[s_phase]   = solve_Kinetic_Coupling(region, F, E, isKinetic, MM, invDX, V, DV, invDV, dt, m2, epsilon, inveps)
            # Project Kinetic to fluid
            rhon[s_space] = ProjToFluid(Fn[s_phase], DV[s_phase])
        else: # Use fluid limit
            rhon[s_space] = solve_Fluid_Coupling(region, rho, dxH, dt)
            # Lift fluid to Kinetic
            Fn[s_phase]   = liftToKin(region, rho, H, dxH, E, V, MM, invDX, epsilon, m2, order)
            
    
    # \dx(\rho) - E*\rho
    H[0]    = (rhon[1]  - rhon[lg])*invDX[0,0]*0.5 - E[0,0]*rhon[0]
    H[1:-1] = (rhon[2:] - rhon[:-2])*invDX[0,1:-1]*0.5  - E[0,1:-1]*rhon[1:-1]
    H[-1]   = (rhon[rg] - rhon[-2])*invDX[0,-1]*0.5 - E[0,-1]*rhon[-1]

    dxH = Calc_dxH(rhon, E[0,:], invDX[0,:], m2)
    L1G = np.sum(DV*np.abs(Fn - np.tile(rhon.T,(Nv,1))*MM),axis=0)
    
    indicator = get_indicator(H, E[0,:], dxE[0,:], invDX[0,:])
    # Update the state of the cells 
    if Coupling:
        isKineticn, regionsn = updateState(indicator, L1G, isKinetic, eta, delta, epsilon)
    else:
        isKineticn, regionsn = isKinetic, regions

    return (Fn, rhon, H, dxH, indicator, isKineticn, regionsn)
""" Brief : Module containing usefull discretization and post-treatment functions
    Author : Laidin Tino, Université de Nantes
    Date : July 2021
"""

##################################################################
#                            TOOLS                               #
##################################################################

import numpy as np
import scipy as sp
import scipy.sparse
import scipy.sparse.linalg
import matplotlib.pyplot as plt
import time

# ------------------------------------------------------------- #
         ##### DISCRETIZATION OF THE PHASE SPACE #####
# ------------------------------------------------------------- #
def discretizeV(L,vstar):
    """ Brief: Creates a discretization of the symmetric velocity domain [-vstar, vstar] with 2L cells.
    
    Input: 
    - vstar: Right endpoint
    - L:     Number of cells with positive velocities
    
    Output: 
    - v:     Cell centers
    - vhalf: Cell interfaces
    - dv:    Cell lengths
    - Nv:    Number of cells
    - vmin:  Left endpoint
    - vmax:  Right endpoint
    """
    
    vmax = vstar
    vmin = -vstar
    Nv = 2 * L
    vhalf = np.linspace(vmin,vmax, Nv+1) # Cell interface points
    dv = np.diff(vhalf) # Cell length
    v = vhalf[:len(vhalf)-1] + dv*.5 # Cell centers
    return (v,vhalf,dv,Nv,vmin,vmax)

def discretizeX(Nx,X):
    """ Brief: Creates a discretization of the periodic space domain of length X with Nx cells. Warning: if Nx is even it is changed to Nx+1.
    
    Input: 
    - X:  Length of the domain
    - Nx: Number of cells
    
    Output: 
    - x:     Cell centers
    - xhalf: Cell interfaces
    - dx:    Cell length
    - Nx:    Number of cells
    - xmin:  left endpoint
    - xmax:  right endpoint
    """
    
    if Nx % 2 == 0:
        Nx = Nx + 1

    xmin = 0
    xmax = X

    xhalf = np.linspace(xmin,xmax, Nx + 1) # Cell interface points
    dx = np.diff(xhalf) # Cell lengths
    invdx = 1.0/dx
    xhalf = xhalf[:len(xhalf)-1] # Interface points (last interface forgotten by periodicity)
    x = xhalf + dx*0.5  # Cell centers
    return (x,xhalf,dx,invdx,Nx,xmin,xmax)

def discretizeXV(x,Nx,dx,invdx,v,Nv,dv):
    """ Brief: Puts the discretization of the phase space in meshgrid (matrix) form. Rows are velocities and columns are positions.
    
    Input: 
    - x:     Cell centers in position
    - Nx:    Number of cells in position
    - dx:    Lengths of cells in position
    - invdx: One over lengths of cells in position
    - v:     Cell centers in position
    - Nv:    Number of cells in velocities
    - dv:    Lengths of cells in velocities
    
    Output: 
    - X:     Cell centers in position              (meshgrid form)
    - DX:    Lengths of cells in position          (meshgrid form)
    - invDX: One over lengths of cells in position (meshgrid form)
    - V:     Cell centers in position              (meshgrid form)
    - DV:    Lengths of cells in velocities        (meshgrid form)
    - N:     Number of cells in phase space
    """
    
    [X,V] = np.meshgrid(x,v)
    DV = np.transpose(np.tile(dv,(Nx,1)))
    DX = np.tile(dx,(Nv,1))
    invDX = np.tile(invdx,(Nv,1))
    invDV = 1.0/DV
    N = Nx * Nv
    return (X,V,DV,invDV,DX,invDX,N)

# ------------------------------------------------------------- #
                ##### ELECTRICAL FIELD #####
# ------------------------------------------------------------- #
from scipy.integrate import solve_ivp
def E_Null(x):
    """ Brief: Null electrical field """
    return (0*x, 0*x, 0*x)

def E_Quadratic(x, const):
    """ Brief: Quadratic electrical field """
    return (const*x*(x-xstar),const*(2*x-xstar), const*(x*x*x/3 + x*x*xstar/2))

def E_Cosinus(x, xstar):
    """ Brief: Cosinus electrical field """
    per = 2
    amp = 1.0
    return (amp*np.cos(per*np.pi*x/xstar),
            amp*-per*np.pi/xstar*np.sin(per*np.pi*x/xstar), 
            amp*np.sin(per*np.pi*x/xstar)/(per*np.pi/xstar))

def E_Gaussian(x,xstar,dx):
    """ Brief: Gaussian electrical field """
    
    F = lambda x, s: np.exp(-(x-xstar*0.5)*(x-xstar*0.5)*0.5*10)/np.sqrt(2*np.pi)*10
    intF= solve_ivp(F, [0, xstar], [F(0,0)], t_eval=x[0,:], method='DOP853',rtol = 1e-13, atol = 1e-13)
    return (F(X,0),
            (x-xstar*0.5)*F(X,0)/np.sqrt(2*np.pi)*100,
            np.tile(intF.y[0]/np.sqrt(2*np.pi)*10,(Nv,1)) )

# ------------------------------------------------------------- #
                ##### DISCRETE MAXWELLIANS #####
# ------------------------------------------------------------- #
def gaussian(v):
    """ Brief: Gaussian distribution """
    return np.exp(-v*v*0.5) / np.sqrt(2.0 * np.pi)

def one(v):
    """ Brief: cst """
    return np.ones(len(v))

def Maxwellian(dist, v, vhalf, dv, Nx):
    """ Brief : Computes the Maxwellian distribution from the velocity discretization.
    
    Input : 
    - dist:  distribution function
    - v:     cell centers
    - vhalf: interface points
    - dv:    cell length
    - Nx:    number of cells in x 
    
    Output:
    - M: Maxwellian at cell center 
    (- Mhalf: Maxwellian at interfaces)
    - MM: Maxwellian at cell center (meshgrid form)
    """
        
    #BGK
    M = dist(v) # In cells
    MassM = np.sum(M * dv)
    M = M / MassM # Normalize mass
    MM = np.transpose(np.tile(M,(Nx,1)))
    Mhalf = [] # Interfaces are not needed
   
    return (M,Mhalf,MM)

def Maxw_moment(M,i,v,dv):
    """ Brief: Cumputes the i-th moment of the Maxwellian """
    return np.sum(v**i*M*dv)

# ------------------------------------------------------------- #
                    ###### INITIAL DATA ######
# ------------------------------------------------------------- #
def init_Gaussian(X,V,xstar):
    """ Brief: Centered Gaussian """
    F = gaussian(V)*gaussian((X-np.pi/2)/0.3)
    return F

def init_Oscillation(X,V,xstar):
    """ Brief: Perturbed Gaussian """
    F = gaussian(V)* (1. + 1.*np.cos(2 * np.pi * X / xstar))
    return F

def init_ball(X,V,xstar,vstar):
    """ Brief: indicator function of an ellipse """
    F = 1.*((X-0.5*xstar)**2+(V/2/vstar)**2 < 0.2**2)
    return F 

def init_other(X,V,xstar):
    """ Brief: Non-uniform in x, non-gaussian in v distribution """
    F =  gaussian(V)*V**4*(1+1*np.cos(2 * np.pi * X / xstar))
    return F
    
def initAll(F, Finf, MM, DV, mf):
    """ Brief: Computes all useful macroscopic and microscopic quantities from F and Finf
    
     Input: 
     - F:       transient state (meshgrid form)
     - Finf:    Steady state    (meshgrid form)
     - MM:      Maxwelllian     (meshgrid form)
     - DV:      Cell length     (meshgrid form)
     - mf:      Mass
     - epsilon: Knudsen number
    
     Output: 
     - Rho: macroscopic density (meshgrid form)
     - G:   microscopic unknown (meshgrid form)
    """

    Nv = F.shape[0]
    Nx = F.shape[1]
    rho = np.transpose(np.sum(F * DV,0))
    Rho = np.tile(rho,(Nv,1))
    G = F - Rho*MM
    return (Rho,G)

def Equilibrium(F,MM,E,intE,DX,DV,xstar):
    """ Brief: Computes the equilibrium
    
    Input:
    - F:     transient state (meshgrid form)
    - MM:    Maxwellian      (meshgrid form)
    - DX,DV: Cell length     (meshgrid form)
    - xstar: length of the space dommain
    
    Output:
    - mf: Mass
    - Finf: Steady state in meshgrid form
    """

    Finf = MM * np.exp(intE)
    mf = np.sum(Finf * DX * DV) / xstar
    return (Finf/mf,mf)

def CFL(dx,dv,m2,Emax,vstar,eps,C):
    dtLim = 2*C*min(dx*dx/m2, dx/Emax)
    dtMM  = 4*C*C*C*min(dx*dx/vstar/vstar,dv*dv/Emax/Emax)
    dt    = min(dtLim, dtMM)
    return (dt,dtLim,dtMM)

def Calc_dx(rho, invdx):
    dxrho = np.zeros(rho.shape)
    dxrho[0]    = rho[1]  - rho[-1]
    dxrho[1:-1] = rho[2:] - rho[:-2]
    dxrho[-1]   = rho[0] - rho[-2]
    dxrho *= invdx*0.5
    return dxrho

# ------------------------------------------------------------- #
                  ##### POST TREATMENT #####
# ------------------------------------------------------------- #
def diagnostic(F, G, Rho, fluid, MM, DV, DX, mf):
    """ Brief: Compute control variates """
    
    L2f = np.sum( (F-mf*MM) * (F-mf*MM)/MM * DV * DX)
    L2G = np.sum(G * G/MM * DV * DX)
    L2Rho = np.sum( (Rho[0,:]-mf) * (Rho[0,:]-mf) * DX[0,:])
    L2fluid = np.sum( (fluid-mf) * (fluid-mf) * DX[0,:])
    return (np.sqrt(L2f), np.sqrt(L2G), np.sqrt(L2Rho), np.sqrt(L2fluid))
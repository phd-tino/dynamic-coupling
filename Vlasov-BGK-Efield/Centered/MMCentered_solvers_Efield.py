""" Brief : Module containing usefull functions and Finite Differences solvers for Micro-Macro kinetic, fluid and hybrid method
    Author : Laidin Tino, Université de Nantes
    Date : July 2021
"""

from MMCentered_tools_Efield import *

##################################################################
#                      NON COUPLED SOLVERS                       #
##################################################################
def solve_MicroMacro(Rho, G, E, MM, invDX, V, DV, invDV, dt, m2, eps):
    """ Brief: Solves the Vlasov equation using the micro-macro model
    
    Input :
    - Rho:    Rho at t^n                       (meshgrid form)
    - G:      G   at t^n                       (meshgrid form)
    - E:      Exterior electrical field        (meshgrid form)
    - MM:     Maxwellian                       (meshgrid form)
    - invDX:  One over cell length in position (meshgrid form)
    - V:      Cell centers in velocities       (meshgrid form)
    - DV:     Cell length in velocity          (meshgrid form)
    - m2:     2nd Maxwellian moment
    - dt:     Time step
    - eps:    Knudsen number
    
    Output :
    - G_new:   G   at t^n+1 (meshgrid form)
    - Rho_new: Rho at t^n+1 (meshgrid form)
    """
    
    Nv = G.shape[0]
    Nx = G.shape[1]
    
    # Ghost cells
    llghost = -2
    lghost  = -1
    rghost  = 0
    rrghost = 1
    
    # Memory allocation
    gflux_s = np.zeros((Nv,Nx+1))
    gflux_v = np.zeros((Nv+1,Nx))
    
    Mflux   = np.zeros((Nv,Nx))
    phi     = np.zeros((Nv,Nx))
    psi     = np.zeros((Nv,Nx))
    phiflux = np.zeros((Nv,Nx))
    psiflux = np.zeros((Nv,Nx))
    rhoflux = np.zeros((Nv,Nx))
    diffu   = np.zeros((Nv,Nx))
    Erho    = E*Rho
    drift   = np.zeros((Nv,Nx))
    
    tmp     = np.zeros((Nv,Nx))
    
    # ----- Computes G_new -----
    # Downstream flux on g in space
    gflux_s[:,0]    = G[:,0] - G[:,lghost]
    gflux_s[:,1:-1] = np.diff(G,axis=1)
    gflux_s[:,-1]   = G[:,rghost] - G[:,-1]
    
    # Downstream flux on g in velocity
    gflux_v[0,:]    = 0.0 # Homogeneous Neumann
    gflux_v[1:-1,:] = np.diff(G,axis=0)
    gflux_v[-1,:]   = 0.0 # Homogeneous Neumann
    
    # Upwind flux for the space derivative
    phi = (V>=0)*V*gflux_s[:,:-1] + (V<0)*V*gflux_s[:,1:]
    phi *= invDX
    
    # Upwind flux for the velocity derivative
    psi = (E>=0)*E*gflux_v[:-1,:] + (E<0)*E*gflux_v[1:,:]
    psi *= invDV
    
    # Centered difference on M in velocity
    Mflux[0,:]    = 0 # Homogeneous Neumann
    Mflux[1:-1,:] = MM[2:,:] - MM[:-2,:]
    Mflux[-1,:]   = 0 # Homogeneous Neumann
    Mflux *= invDV*0.5
    
    # Centered difference on rho in space
    rhoflux[:,0]    = Rho[:,1] - Rho[:,lghost]
    rhoflux[:,1:-1] = Rho[:,2:] - Rho[:,:-2]
    rhoflux[:,-1]   = Rho[:,rghost] - Rho[:,-2]
    rhoflux *= invDX*0.5
    
    G_new =  eps*(-phi + MM*np.tile(np.sum(DV*phi, axis=0).T,(Nv,1)) - psi - rhoflux*V*MM - Erho*Mflux)
    G_new += (eps*eps/dt)*G + eps*G_new
    G_new /= (1.0 + eps*eps/dt)
    
    # ----- Computes Rho_new -----   
    # Computes phi at first left and right ghostcells
    phi_lg = (V[:,lghost]>=0)*V[:,lghost]*(G[:,lghost] - G[:,llghost]) + (V[:,0]<0)*V[:,0]*gflux_s[:,0]
    phi_lg *= invDX[:,lghost]
    phi_rg = (V[:,-1]>=0)*V[:,-1]*gflux_s[:,-1] + (V[:,rghost]<0)*V[:,rghost]*(G[:,rrghost]-G[:,rghost])
    phi_rg *= invDX[:,rghost]
    
    psi_lg = psi[:,lghost]
    psi_rg = psi[:,rghost]
    
    # Centered difference on phi
    phiflux[:,0]    = phi[:,1] - phi_lg
    phiflux[:,1:-1] = phi[:,2:] - phi[:,:-2]
    phiflux[:,-1]   = phi_rg - phi[:,-2]
    
    # Centered difference on psi
    psiflux[:,0]    = psi[:,1] - psi_lg
    psiflux[:,1:-1] = psi[:,2:] - psi[:,:-2]
    psiflux[:,-1]   = psi_rg - psi[:,-2]
    
    # Diffusiv term
    diffu[:,0]    = Rho[:,llghost] - 2.0*Rho[:,0]    + Rho[:,2]
    diffu[:,1]    = Rho[:,lghost]  - 2.0*Rho[:,1]    + Rho[:,3]
    diffu[:,2:-2] = Rho[:,:-4]     - 2.0*Rho[:,2:-2] + Rho[:,4:]
    diffu[:,-2]   = Rho[:,-4]      - 2.0*Rho[:,-2]   + Rho[:,rghost]
    diffu[:,-1]   = Rho[:,-3]      - 2.0*Rho[:,-1]   + Rho[:,rrghost]
    diffu *= invDX*0.5
    
    # Drift term
    drift[:,0]    = Erho[:,1] - Erho[:,lghost]
    drift[:,1:-1] = Erho[:,2:] - Erho[:,:-2]
    drift[:,-1]   = Erho[:,rghost] - Erho[:,-2]
    
    # Centered difference on g
    tmp[:,0]    = G[:,1] - G[:,lghost]
    tmp[:,1:-1] = G[:,2:] - G[:,:-2]
    tmp[:,-1]   = G[:,rghost] - G[:,-2]
    
    # term to integrate with respect to v
    tmp = V*(eps/dt*tmp - phiflux - psiflux)
    tmp = np.tile(np.transpose(np.sum(DV*tmp, axis=0)),(Nv,1)) - m2*diffu + drift
    Rho_new = Rho - dt/(eps*eps/dt+1)*0.5*invDX*tmp
    
    F_new = Rho_new*MM + G_new
    return (Rho_new,G_new,F_new)

def solve_Fluid(fluid, E, alpha, invdx, dt):
    """ Brief: Solves the diffusion limit with a diffusion coefficient alpha (large stencil)
    
     Input :
     - fluid:  Rho_fluid at t^n
     - E:      Exterior electrical field
     - alpha:  Diffusion coefficient
     - invdx:  Inverse of the space discretization
     - dt:     Time step
    
     Output :
     - fluid_new: updated heat
    """
    
    Nx = fluid.shape[0]
    # Ghost cells
    llghost = -2
    lghost  = -1
    rghost  = 0
    rrghost = 1
    
    # Memory allocation
    diffu = np.zeros(Nx)
    drift = np.zeros(Nx)
    Erho  = E*fluid
    
    diffu[0]    = fluid[llghost] - 2.0*fluid[0]    + fluid[2]
    diffu[1]    = fluid[lghost]  - 2.0*fluid[1]    + fluid[3]
    diffu[2:-2] = fluid[:-4]     - 2.0*fluid[2:-2] + fluid[4:]
    diffu[-2]   = fluid[-4]      - 2.0*fluid[-2]   + fluid[rghost]
    diffu[-1]   = fluid[-3]      - 2.0*fluid[-1]   + fluid[rrghost]
    
    drift[0]    = Erho[1] - Erho[lghost]
    drift[1:-1] = Erho[2:] - Erho[:-2]
    drift[-1]   = Erho[rghost] - Erho[-2]
    
    return  fluid + alpha*dt*0.25*invdx*invdx*diffu - dt*0.5*invdx*drift

##################################################################
#                      DYNAMIC COUPLING                          #
################################################################## 

#  --------------- COUPLING TOOLS --------------------
def indicators(rho_kin, rho_fluid, E, dxE, invdx):
    
    llg = -2
    lg  = -1
    rg  = 0
    rrg = 1
    
    indKinetic = np.zeros(invdx.shape[0])
    indFluid   = np.zeros(invdx.shape[0])
    
    H    = np.zeros(invdx.shape[0])
    H[0]    = (rho_kin[1]  - rho_kin[lg])*invdx[0]*0.5 - E[0]*rho_kin[0]
    H[1:-1] = (rho_kin[2:] - rho_kin[:-2])*invdx[1:-1]*0.5  - E[1:-1]*rho_kin[1:-1]
    H[-1]   = (rho_kin[rg] - rho_kin[-2])*invdx[-1]*0.5 - E[-1]*rho_kin[-1]
    dxH  = Calc_dx(H, invdx)
    dxxH = Calc_dx(dxH,invdx)
    indKinetic = E*dxH + 2.0*H*dxE - dxxH
    
    H[0]    = (rho_fluid[1]  - rho_fluid[lg])*invdx[0]*0.5 - E[0]*rho_fluid[0]
    H[1:-1] = (rho_fluid[2:] - rho_fluid[:-2])*invdx[1:-1]*0.5  - E[1:-1]*rho_fluid[1:-1]
    H[-1]   = (rho_fluid[rg] - rho_fluid[-2])*invdx[-1]*0.5 - E[-1]*rho_fluid[-1]
    dxH  = Calc_dx(H, invdx)
    dxxH = Calc_dx(dxH,invdx)
    indFluid = E*dxH + 2.0*H*dxE - dxxH
    
    return (indKinetic, indFluid)


def updateState(H, dxH, E, dxE, L1G, isKinetic, eta, delta, eps, invdx):
    """ Brief: Computes the new kinetic and fluid cells of the domain based on their current state
    
    Input:
    - H:         dx(rho) + E*rho
    - dxH:       Drift diffusion term
    - E:         Confining electrical field
    - dxE:       dx(E)
    - L1G:       L1 norm of  G at t^n : ||g(t^n, x_i+.5, . )||_L1
    - isKinetic: Array of boolean, each element corresponds to the state of a cell
    - eta:       Threshold determining the change from fluid to kinetic
    - delta:     Threshold determining the change from kinetic to fluid
    - eps:       Knudsen number
    - invdx:     
    
    Output:
    - new_state: Boolean list of updated cells' state
    """
    
    Nx = H.shape[0]
    dxxH = np.zeros(Nx)
    dxxH[0]    = dxH[1] - dxH[-1]
    dxxH[1:-1] = dxH[2:] - dxH[:-2]
    dxxH[-1]   = dxH[0] - dxH[-2]
    dxxH *= invdx*0.5
    
    crit = abs(2*H*dxE - E*dxH - dxxH)
    
    toFluid   = np.full(Nx,True, dtype=bool)
    toKinetic = np.full(Nx,True, dtype=bool)
     
    toFluid = toFluid & (L1G  < delta)
    toFluid = toFluid & (crit <= eta)   # Stays fluid criterion
    
    toKinetic = crit>eta
    
    # If Kinetic and tofluid, flip ; If Fluid and toKinetic, flip
    newState = np.where( (isKinetic & toFluid) | ( ~isKinetic & toKinetic), ~isKinetic, isKinetic)
    
    # Storing regions with two ghost cells on each side
    regions = [[-2,-1,0]] # left ghostcell by periodicity and first cell
    prev = newState[0]
    count = 0
    for i in range(1,Nx-1):
        curr = newState[i]
        if curr == prev:
            regions[count].append(i)
        else:
            if i != Nx-2:
                regions[count].extend([i,i+1])  # Right ghostcells of current region
            else:
                regions[count].extend([i+1,-1])   # Right ghostcells of current region
            count += 1                            # Pass to the next region
            regions.append([i-2,i-1, i])          # First cell of next region and left ghostcells
        prev = curr
        
    # Dealing with the last cell 
    curr = newState[Nx-1]
    if  curr == prev:
        regions[count].extend([Nx-1,0,1]) # last cell and right ghostcells by periodicity
    else:
        regions[count].extend([Nx-1, 0]) # Right ghostcells of the second to last region
        regions.append([Nx-3, Nx-2, Nx-1, 0, 1]) # Last region is the single last cell
    
    return (newState, regions)

#  ------------ ADAPTED LOCAL SOLVERS ----------------

def solve_Fluid_Coupling(region, Rho, dxH, Nv, dt):
    """ Brief: Solves the diffusion limit with a diffusion coefficient alpha (wide stencil)
    
     Input :
     - region: Fluid subdomain
     - Rho:    Rho at t^n
     - E:      Exterior electrical field
     - alpha:  Diffusion coefficient
     - invdx:  Inverse of the space discretization
     - dt:     Time step
    
     Output :
     - fluid_new: updated heat
    """
    
    Nx = len(region)-4
    s  = np.s_[:,region[2]:region[-3]+1]
    Gn = np.zeros((Nv,Nx))
    return  (Rho[s] + np.tile( np.transpose(dt*dxH) ,(Nv,1)), Gn)

def solve_MM_Coupling(region, Rho, G, dxH, E, MM, invDX, V, DV, invDV, dt, m2, eps, isKinetic, order):
    """ Brief: Solves the Vlasov equation using the micro-macro model
    
    Input :
    - Rho:    Rho at t^n                       (meshgrid form)
    - G:      G   at t^n                       (meshgrid form)
    - dxH     dx( dx\rho - E\rho )             (meshgrid form)
    - E:      Exterior electrical field        (meshgrid form)
    - MM:     Maxwellian                       (meshgrid form)
    - invDX:  One over cell length in position (meshgrid form)
    - V:      Cell centers in velocities       (meshgrid form)
    - DV:     Cell length in velocity          (meshgrid form)
    - m2:     2nd Maxwellian moment
    - dt:     Time step
    - eps:    Knudsen number
    - isKinetic: Array of the current state of the cells (Boolean)
    - order:
    
    Output :
    - G_new:   G   at t^n+1 (meshgrid form)
    - Rho_new: Rho at t^n+1 (meshgrid form)
    """
    
    Nv = G.shape[0]
    Nx = len(region)-4
    
    # Ghost cells
    llghost = region[0]
    lghost  = region[1]
    first   = region[2]
    last    = region[-3]
    rghost  = region[-2]
    rrghost = region[-1]
    
    # Slices
    s   = np.s_[:, first:last+1] # Local slice
    im1 = np.s_[:,first:last-1]   # Local slice i-1
    ip1 = np.s_[:,first+2:last+1] # Local slice 1+1
    
    # Memory allocation
    gflux_s = np.zeros((Nv,Nx+1))
    gflux_v = np.zeros((Nv+1,Nx))
    tmppsi = np.zeros(Nv+1)
    
    Mflux   = np.zeros((Nv,Nx))
    phi     = np.zeros((Nv,Nx))
    psi     = np.zeros((Nv,Nx))
    tmpflux = np.zeros((Nv,Nx))
    rhoflux = np.zeros((Nv,Nx))
    
    # Centered difference on M in velocity
    Mflux[0,:]    = 0 # Homogeneous Neumann
    Mflux[1:-1,:] = MM[2:,first:last+1] - MM[:-2,first:last+1]
    Mflux[-1,:]   = 0 # Homogeneous Neumann
    Mflux *= invDV[s]*0.5
    
    # Dealing with the state of ghost cells
    if order == 1 :
        if isKinetic[llghost]:
            G_llg = G[:,llghost]
        else:
            G_llg = -eps*V[:,llghost]*MM[:,llghost]*(Rho[:,lghost]-Rho[:,llghost-1])*0.5*invDX[:,llghost]
            G_llg -= eps*E[:,llghost]*Rho[:,llghost]*Mflux[:,0]
        if isKinetic[lghost]:
            G_lg  = G[:,lghost]
        else:
            G_lg  = -eps*V[:,lghost]*MM[:,lghost]*(Rho[:,llghost]-Rho[:,first])*0.5*invDX[:,lghost]
            G_lg -= eps*E[:,lghost]*Rho[:,lghost]*Mflux[:,0]
        if isKinetic[rghost]:
            G_rg  = G[:,rghost]
        else:
            G_rg  = -eps*V[:,rghost]*MM[:,rghost]*(Rho[:,rrghost]-Rho[:,last])*0.5*invDX[:,rghost]
            G_rg -= eps*E[:,rrghost]*Rho[:,rrghost]*Mflux[:,0]
        if isKinetic[rrghost]:
            G_rrg = G[:,rrghost]
        else:
            G_rrg = -eps*V[:,rrghost]*MM[:,rrghost]*(Rho[:,rghost]-Rho[:,(rrghost+1)%G.shape[1]])*0.5*invDX[:,rrghost]
            G_rrg -= eps*E[:,rrghost]*Rho[:,rrghost]*Mflux[:,0]
    else:
        G_llg = G[:,llghost]
        G_lg  = G[:,lghost]
        G_rg  = G[:,rghost]
        G_rrg = G[:,rrghost]
    
    # ----- Computes G_new -----
    # Downstream flux on g in space
    gflux_s[:,0]    = G[:,first] - G_lg
    gflux_s[:,1:-1] = np.diff(G[s],axis=1)
    gflux_s[:,-1]   = G_rg - G[:,last]
    
    # Downstream flux on g in velocity
    gflux_v[0,:]    = 0.0 # Homogeneous Neumann
    gflux_v[1:-1,:] = np.diff(G[s],axis=0)
    gflux_v[-1,:]   = 0.0 # Homogeneous Neumann
    
    # Upwind flux for the space derivative
    phi = (V[s]>=0)*V[s]*gflux_s[:,:-1] + (V[s]<0)*V[s]*gflux_s[:,1:]
    phi *= invDX[s]
    
    # Upwind flux for the velocity derivative
    psi = (E[s]>=0)*E[s]*gflux_v[:-1,:] + (E[s]<0)*E[s]*gflux_v[1:,:]
    psi *= invDV[s]
    

    
    # Centered difference on rho in space
    if Nx != 1:
        rhoflux[:,0]    = Rho[:,first+1] - Rho[:,lghost]
        rhoflux[:,1:-1] = Rho[ip1] - Rho[im1]
        rhoflux[:,-1]   = Rho[:,rghost] - Rho[:,last-1]
    else:
        rhoflux[:,0]  = Rho[:,rghost] - Rho[:,lghost]
    rhoflux *= invDX[s]*0.5
    
    G_new =  eps*(MM[s]*np.tile(np.sum(DV[s]*phi, axis=0).T,(Nv,1)) - phi - psi - rhoflux*V[s]*MM[s] - E[s]*Rho[s]*Mflux)
    G_new += (eps*eps/dt)*G[s] + eps*G_new
    G_new /= (1.0 + eps*eps/dt)
    
    # ----- Computes Rho_new -----   
    # Computes phi at first left and right ghostcells
    phi_lg = (V[:,lghost]>=0)*V[:,lghost]*(G_lg - G_llg) + (V[:,0]<0)*V[:,0]*gflux_s[:,0]
    phi_lg *= invDX[:,lghost]
    phi_rg = (V[:,-1]>=0)*V[:,-1]*gflux_s[:,-1] + (V[:,rghost]<0)*V[:,rghost]*(G_rrg - G_rg)
    phi_rg *= invDX[:,rghost]
    
    # Computes psi at first left and right ghostcells
    # Downstream flux on g in velocity at left ghost cell
    tmppsi[0]    = 0.0 # Homogeneous Neumann
    tmppsi[1:-1] = np.diff(G_lg, axis=0)
    tmppsi[-1]   = 0.0 # Homogeneous Neumann
    psi_lg = (E[:,lghost]>=0)*E[:,lghost]*tmppsi[1:] + (E[:,lghost]<0)*E[:,lghost]*tmppsi[:-1]
    psi_lg *= invDV[:,lghost]
    
    # Downstream flux on g in velocity at right ghostcell
    tmppsi[0]    = 0.0 # Homogeneous Neumann
    tmppsi[1:-1] = np.diff(G_rg, axis=0)
    tmppsi[-1]   = 0.0 # Homogeneous Neumann
    psi_rg = (E[:,rghost]>=0)*E[:,rghost]*tmppsi[1:] + (E[:,rghost]<0)*E[:,rghost]*tmppsi[:-1]
    psi_rg *= invDV[:,rghost]
    
    # Centered difference on g, phi and psi
    if Nx != 1:
        tmpflux[:,0]    = eps/dt*(G[:,1] - G_lg)   - (phi[:,1] - phi_lg)   - (psi[:,1] - psi_lg)
        tmpflux[:,1:-1] = eps/dt*(G[ip1] - G[im1]) - (phi[:,2:] - phi[:,:-2]) - (psi[:,2:] - psi[:,:-2])
        tmpflux[:,-1]   = eps/dt*(G_rg - G[:,-2])  - (phi_rg - phi[:,-2])  - (psi_rg - psi[:,-2])
    else:
        tmpflux[:,0]    = eps/dt*(G_rg - G_lg) - (phi_rg - phi_lg) - (psi_rg - psi_lg)
    
    tmp = np.tile(np.transpose(np.sum(DV[s]*V[s]*tmpflux, axis=0)),(Nv,1))*0.5*invDX[s] - dxH[s]
    Rho_new = Rho[s] - dt/(eps*eps/dt+1)*tmp
    
    return (Rho_new,G_new)

#  ------------ GLOBAL COUPLED SOLVER ----------------

def solve_Coupled(Rho, G, E, dxE, isKinetic, regions, eta, delta, 
                  MM, invDX, V, DV, invDV, dt, m2, m4, epsilon,Coupling,order):
    """ Brief: Solves the Vlasov equation at time t using a dynamic coupling method
    
    Input :
    - Rho:       Rho at t^n                       (meshgrid form)
    - G:         G   at t^n                       (meshgrid form)
    - E:         Confining electrical field       (meshgrid form)
    - dxE:       dx(E)
    - isKinetic: Array of the current state of the cells (Boolean)
    - regions:   List of list containing a given region indexes
    - MM:        Maxwellian                       (meshgrid form)
    - eta:       Threshold determining the change from fluid to kinetic
    - delta:     Threshold determining the change from kinetic to fluid
    - invDX:     One over cell length in position (meshgrid form)
    - V:         Cell centers in velocities       (meshgrid form)
    - DV:        Cell length in velocity          (meshgrid form)
    - m2:        2nd Maxwellian moment
    - eps:       Knudsen number
    - Coupling:  Boolean that enables dynamic coupling
    
    Output :
    - G_new:      G   at t^n+1 (meshgrid form)
    - Rho_new:    Rho at t^n+1 (meshgrid form)
    - isKineticn: Updated state
    - regionsn:   Updated regions
    """
    
    Nv = G.shape[0]
    Nx = G.shape[1]
    
    # Specific cells
    llghost = -2
    lghost  = -1
    rghost  = 0
    rrghost = 1
    
    # Memory allocation
    Rhon  = np.zeros((Nv,Nx))
    Gn    = np.zeros((Nv,Nx))
    Fn    = np.zeros((Nv,Nx))
    H     = np.zeros((Nv,Nx))
    dxH   = np.zeros((Nv,Nx))
    dvM   = np.zeros((Nv,Nx))
    
    # Drift-diffusion term
    H[:,0]    = (Rho[:,1] - Rho[:,lghost])*invDX[:,0]*0.5   - E[:,0]*Rho[:,0]
    H[:,1:-1] = (Rho[:,2:] - Rho[:,:-2])*invDX[:,1:-1]*0.5  - E[:,1:-1]*Rho[:,1:-1]
    H[:,-1]   = (Rho[:,rghost] - Rho[:,-2])*invDX[:,-1]*0.5 - E[:,-1]*Rho[:,-1]   
    
    # Diffusiv term
    dxH[:,0]    = Rho[:,llghost] - 2.0*Rho[:,0]    + Rho[:,2]
    dxH[:,1]    = Rho[:,lghost]  - 2.0*Rho[:,1]    + Rho[:,3]
    dxH[:,2:-2] = Rho[:,:-4]     - 2.0*Rho[:,2:-2] + Rho[:,4:]
    dxH[:,-2]   = Rho[:,-4]      - 2.0*Rho[:,-2]   + Rho[:,rghost]
    dxH[:,-1]   = Rho[:,-3]      - 2.0*Rho[:,-1]   + Rho[:,rrghost]
    dxH *= m2*invDX*0.5
    
    # Drift term
    dxH[:,0]    -= E[:,1]*Rho[:,1] - E[:,lghost]*Rho[:,lghost]
    dxH[:,1:-1] -= E[:,2:]*Rho[:,2:] - E[:,:-2]*Rho[:,:-2]
    dxH[:,-1]   -= E[:,rghost]*Rho[:,rghost] - E[:,-2]*Rho[:,-2]
    dxH *= invDX*0.5
    
    # L1 norm of G 
    L1G = np.sum(DV*np.abs(G),axis=0)
    
    # Update micro macro unknwns
    for region in regions: # Solve per inner regions with appropriate boundary conditions
        zone = np.s_[:, region[2]:region[-3]+1]
        if isKinetic[region[2]]: # Use kinetic model
            (Rhon[zone],Gn[zone]) = solve_MM_Coupling(region, Rho, G, dxH, E, MM, invDX, V, DV, invDV, dt, m2, epsilon, isKinetic, order)
        else: # Use fluid limit
            (Rhon[zone],Gn[zone]) = solve_Fluid_Coupling(region, Rho, dxH[0,region[2]:region[-3]+1], Nv, dt)

    # Update F
    Fn = Rhon*MM + Gn
    
    # Drift-diffusion term
    H[:,0]    = (Rho[:,1] - Rho[:,lghost])*invDX[:,0]*0.5   - E[:,0]*Rho[:,0]
    H[:,1:-1] = (Rho[:,2:] - Rho[:,:-2])*invDX[:,1:-1]*0.5  - E[:,1:-1]*Rho[:,1:-1]
    H[:,-1]   = (Rho[:,rghost] - Rho[:,-2])*invDX[:,-1]*0.5 - E[:,-1]*Rho[:,-1]   
    
    # Diffusiv term
    dxH[:,0]    = Rho[:,llghost] - 2.0*Rho[:,0]    + Rho[:,2]
    dxH[:,1]    = Rho[:,lghost]  - 2.0*Rho[:,1]    + Rho[:,3]
    dxH[:,2:-2] = Rho[:,:-4]     - 2.0*Rho[:,2:-2] + Rho[:,4:]
    dxH[:,-2]   = Rho[:,-4]      - 2.0*Rho[:,-2]   + Rho[:,rghost]
    dxH[:,-1]   = Rho[:,-3]      - 2.0*Rho[:,-1]   + Rho[:,rrghost]
    dxH *= m2*invDX*0.5
    
    # Drift term
    dxH[:,0]    -= E[:,1]*Rho[:,1] - E[:,lghost]*Rho[:,lghost]
    dxH[:,1:-1] -= E[:,2:]*Rho[:,2:] - E[:,:-2]*Rho[:,:-2]
    dxH[:,-1]   -= E[:,rghost]*Rho[:,rghost] - E[:,-2]*Rho[:,-2]
    dxH *= invDX*0.5
    
    # Update the state of the cells 
    if Coupling:
        (isKineticn, regionsn) = updateState(H[0,:], dxH[0,:], E[0,:], dxE[0,:], L1G, isKinetic, eta, delta, epsilon, invDX[0,:])
    else:
        isKineticn, regionsn = isKinetic, regions
    return (Rhon,Gn,Fn,isKineticn,regionsn)
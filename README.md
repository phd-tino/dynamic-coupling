# Dynamic coupling for kinetic equation with a diffusiv limit

This project is an implementation of hybrid kinetic/fluid numerical methods for the semiconductor Boltzmann equation.

The repository is divided into two part:
- Vlasov-BGK       : Case of a null electrical field
- Vlasov-BGK-Efield: Case of a non-zero electrical field

Each part contains two distincts implementations :
- A Finite volume approach with a non-asymptotic preserving scheme
- A Finite differences micro-macro approach with an asymptotic preserving scheme

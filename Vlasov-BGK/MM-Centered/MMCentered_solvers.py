""" Brief : Module containing usefull functions and Finite Differences solvers for Micro-Macro kinetic, fluid and hybrid method
    Author : Laidin Tino, Université de Nantes
    Date : July 2021
"""

from MMCentered_tools import *

##################################################################
#                      NON COUPLED SOLVERS                       #
##################################################################
def solve_MicroMacro(Rho, G, region, MM, invDX, V, DV, dt, m2, eps):
    """ Brief: Solves the Vlasov equation using the micro-macro model
    
    Input :
    - Rho:    Rho at t^n                       (meshgrid form)
    - G:      G   at t^n                       (meshgrid form)
    (- region: List of indexes, spatial domain of computation)
    - MM:     Maxwellian                       (meshgrid form)
    - invDX:  One over cell length in position (meshgrid form)
    - V:      Cell centers in velocities       (meshgrid form)
    - DV:     Cell length in velocity          (meshgrid form)
    - m2:     2nd Maxwellian moment
    - dt:     Time step
    - eps:    Knudsen number
    
    Output :
    - G_new:   G   at t^n+1 (meshgrid form)
    - Rho_new: Rho at t^n+1 (meshgrid form)
    """
    
    Nv = G.shape[0]
    Nx = G.shape[1]
    
    # Ghost cells
    llghost = -2
    lghost  = -1
    rghost  = 0
    rrghost = 1
    
    # Memory allocation
    gflux   = np.zeros((Nv,Nx+1))
    phi     = np.zeros((Nv,Nx))
    phiflux = np.zeros((Nv,Nx))
    rhoflux = np.zeros((Nv,Nx))
    diffu   = np.zeros((Nv,Nx))
    tmp     = np.zeros((Nv,Nx))
    
    # ----- Computes G_new -----
    # Downstream flux on g
    gflux[:,0]    = G[:,0] - G[:,lghost]
    gflux[:,1:-1] = np.diff(G,axis=1)
    gflux[:,-1]   = G[:,rghost] - G[:,-1]
    
    # Upwind flux
    phi = (V>=0)*V*gflux[:,:-1] + (V<0)*V*gflux[:,1:]
    phi *= invDX
    
    # Centered difference on rho
    rhoflux[:,0]    = Rho[:,1] - Rho[:,lghost]
    rhoflux[:,1:-1] = Rho[:,2:] - Rho[:,:-2]
    rhoflux[:,-1]   = Rho[:,rghost] - Rho[:,-2]
    rhoflux *= invDX*0.5
    
    G_new = (eps*eps/dt)*G - eps*phi + eps*MM*np.tile(np.sum(DV*phi, axis=0).T,(Nv,1)) - eps*rhoflux*V*MM
    G_new /= (1.0 + eps*eps/dt)
    
    # ----- Computes Rho_new -----   
    # Compute phi at first left and right ghostcells
    phi_lg = (V[:,lghost]>=0)*V[:,lghost]*(G[:,lghost] - G[:,llghost]) + (V[:,0]<0)*V[:,0]*gflux[:,0]
    phi_lg *= invDX[:,lghost]
    phi_rg = (V[:,-1]>=0)*V[:,-1]*gflux[:,-1] + (V[:,rghost]<0)*V[:,rghost]*(G[:,rrghost]-G[:,rghost])
    phi_rg *= invDX[:,rghost]
    
    # Centered difference on phi
    phiflux[:,0]    = phi[:,1] - phi_lg
    phiflux[:,1:-1] = phi[:,2:] - phi[:,:-2]
    phiflux[:,-1]   = phi_rg - phi[:,-2]
    
    # Diffusiv term
    diffu[:,0]    = Rho[:,llghost] - 2.0*Rho[:,0]    + Rho[:,2]
    diffu[:,1]    = Rho[:,lghost]  - 2.0*Rho[:,1]    + Rho[:,3]
    diffu[:,2:-2] = Rho[:,:-4]     - 2.0*Rho[:,2:-2] + Rho[:,4:]
    diffu[:,-2]   = Rho[:,-4]      - 2.0*Rho[:,-2]   + Rho[:,rghost]
    diffu[:,-1]   = Rho[:,-3]      - 2.0*Rho[:,-1]   + Rho[:,rrghost]
    diffu *= invDX*0.5
    
    # Centered difference on g
    tmp[:,0]    = G[:,1] - G[:,lghost]
    tmp[:,1:-1] = G[:,2:] - G[:,:-2]
    tmp[:,-1]   = G[:,rghost] - G[:,-2]
    
    # term to integrate with respect to v
    tmp = V*(eps/dt*tmp - phiflux)
    tmp = np.tile(np.transpose(np.sum(DV*tmp, axis=0)),(Nv,1)) - m2*diffu
    Rho_new = Rho - dt/(eps*eps/dt+1)*0.5*invDX*tmp
    
    F_new = Rho_new*MM + G_new
    return (Rho_new,G_new,F_new)


def solve_Fluid(fluid, region, alpha, invdx, dt):
    """ Brief: Solves the diffusion limit with a diffusion coefficient alpha (large stencil)
    
     Input :
     - fluid:  Rho_fluid at t^n
     (- region: List of indexes, spatial domain of computation)
     - alpha:  Diffusion coefficient
     - invdx:  Inverse of the space discretization
     - dt:     Time step
    
     Output :
     - fluid_new: updated heat
    """
    
    Nx = fluid.shape[0]
    # Ghost cells
    llghost = -2
    lghost  = -1
    rghost  = 0
    rrghost = 1
    
    # Memory allocation
    fluidn  = np.zeros(Nx)
    
    fluidn[0]    = fluid[llghost] - 2.0*fluid[0]    + fluid[2]
    fluidn[1]    = fluid[lghost]  - 2.0*fluid[1]    + fluid[3]
    fluidn[2:-2] = fluid[:-4]     - 2.0*fluid[2:-2] + fluid[4:]
    fluidn[-2]   = fluid[-4]      - 2.0*fluid[-2]   + fluid[rghost]
    fluidn[-1]   = fluid[-3]      - 2.0*fluid[-1]   + fluid[rrghost]
    
    fluidn = fluid + alpha*dt*0.25*invdx*invdx*fluidn
    return  fluidn

##################################################################
#                      DYNAMIC COUPLING                          #
##################################################################
def indicators(rho_kin, rho_fluid, invdx, m2, m4):
    
    llghost = -2
    lghost  = -1
    rghost  = 0
    rrghost = 1
    
    indKinetic = np.zeros(invdx.shape[0])
    indFluid = np.zeros(invdx.shape[0])
    
    indKinetic[0]    = rho_kin[llghost] - 2.0*rho_kin[0]    + rho_kin[2]
    indKinetic[1]    = rho_kin[lghost]  - 2.0*rho_kin[1]    + rho_kin[3]
    indKinetic[2:-2] = rho_kin[:-4]     - 2.0*rho_kin[2:-2] + rho_kin[4:]
    indKinetic[-2]   = rho_kin[-4]      - 2.0*rho_kin[-2]   + rho_kin[rghost]
    indKinetic[-1]   = rho_kin[-3]      - 2.0*rho_kin[-1]   + rho_kin[rrghost]
    indKinetic *= invdx*0.25*(2.*m2*m2-m4)
    
    indFluid[0]    = rho_fluid[llghost] - 2.0*rho_fluid[0]    + rho_fluid[2]
    indFluid[1]    = rho_fluid[lghost]  - 2.0*rho_fluid[1]    + rho_fluid[3]
    indFluid[2:-2] = rho_fluid[:-4]     - 2.0*rho_fluid[2:-2] + rho_fluid[4:]
    indFluid[-2]   = rho_fluid[-4]      - 2.0*rho_fluid[-2]   + rho_fluid[rghost]
    indFluid[-1]   = rho_fluid[-3]      - 2.0*rho_fluid[-1]   + rho_fluid[rrghost]
    indFluid *= invdx*0.25*(2.*m2*m2-m4)
    
    return (indKinetic, indFluid)

def updateState(dxxrho, L1G, isKinetic, regions, eta, delta, eps, m2, m4):
    """ Brief: Computes the new kinetic and fluid cells of the domain based on their current state
    
    Input:
    - dxxrho:   Discrete 2nd derivative of rho at t^n
    - L1G:      L1 norm of  G at t^n : ||g(t^n, x_i+.5, . )||_L1
    - isKineti: Array of boolean, each element corresponds to the state of a cell
    (- regions:  List of list containing a given region indexes)
    - eta:      Threshold determining the change from fluid to kinetic
    - delta:    Threshold determining the change from kinetic to fluid
    - eps:      Knudsen number
    - m2, m4:   2nd and 4th moment of the maxwellian
    
    Output:
    - new_state: Boolean list of updated cells' state
    """
    Nx = dxxrho.shape[0]
    toFluid   = np.full(Nx,True, dtype=bool)
    toKinetic = np.full(Nx,True, dtype=bool)
     
    toFluid = toFluid & (L1G  < delta)
    toFluid = toFluid & (abs((2.*m2*m2+m4)*dxxrho) <= eta)   # Stays fluid criterion
    
    toKinetic = abs((2*m2*m2+m4)*dxxrho)>eta
    
    # If Kinetic and tofluid, flip ; If Fluid and toKinetic, flip
    newState = np.where( (isKinetic & toFluid) | ( ~isKinetic & toKinetic), ~isKinetic, isKinetic)
    
    # Storing regions with two ghost cells on each side
    regions = [[-2,-1,0]] # left ghostcell by periodicity and first cell
    prev = newState[0]
    count = 0
    for i in range(1,Nx-1):
        curr = newState[i]
        if curr == prev:
            regions[count].append(i)
        else:
            if i != Nx-2:
                regions[count].extend([i,i+1])  # Right ghostcells of current region
            else:
                regions[count].extend([i+1,-1])   # Right ghostcells of current region
            count += 1                            # Pass to the next region
            regions.append([i-2,i-1, i])          # First cell of next region and left ghostcells
        prev = curr
        
    # Dealing with the last cell 
    curr = newState[Nx-1]
    if  curr == prev:
        regions[count].extend([Nx-1,0,1]) # last cell and right ghostcells by periodicity
    else:
        regions[count].extend([Nx-1, 0]) # Right ghostcells of the second to last region
        regions.append([Nx-3, Nx-2, Nx-1, 0, 1]) # Last region is the single last cell
    
    return (newState, regions)

def solve_Coupled(Rho, G, isKinetic, regions, eta, delta, 
                  MM, invDX, V, DV, dt, m2, m4, epsilon,Coupling,order):
    """ Brief: Solves the Vlasov equation at time t using a dynamic coupling method
    
    Input :
    - Rho:       Rho at t^n                       (meshgrid form)
    - G:         G   at t^n                       (meshgrid form)
    - isKinetic: Array of the current state of the cells (Boolean)
    - regions:   List of list containing a given region indexes
    - MM:        Maxwellian                       (meshgrid form)
    - eta:       Threshold determining the change from fluid to kinetic
    - delta:     Threshold determining the change from kinetic to fluid
    - invDX:     One over cell length in position (meshgrid form)
    - V:         Cell centers in velocities       (meshgrid form)
    - DV:        Cell length in velocity          (meshgrid form)
    - m2:        2nd Maxwellian moment
    - eps:       Knudsen number
    - Coupling:  Boolean that enables dynamic coupling
    
    Output :
    - G_new:      G   at t^n+1 (meshgrid form)
    - Rho_new:    Rho at t^n+1 (meshgrid form)
    - isKineticn: Updated state
    - regionsn:   Updated regions
    """
    
    Nv = G.shape[0]
    Nx = G.shape[1]
    
    # Specific cells
    llghost = -2
    lghost  = -1
    rghost  = 0
    rrghost = 1
    
    # Memory allocation
    Rhon  = np.zeros((Nv,Nx))
    Gn    = np.zeros((Nv,Nx))
    Fn    = np.zeros((Nv,Nx))
    diffu = np.zeros((Nv,Nx))
    
    # Computing second derivative at each cell center (wide stencil)
    diffu[:,0]    = Rho[:,llghost] - 2.0*Rho[:,0]    + Rho[:,2]
    diffu[:,1]    = Rho[:,lghost]  - 2.0*Rho[:,1]    + Rho[:,3]
    diffu[:,2:-2] = Rho[:,:-4]     - 2.0*Rho[:,2:-2] + Rho[:,4:]
    diffu[:,-2]   = Rho[:,-4]      - 2.0*Rho[:,-2]   + Rho[:,rghost]
    diffu[:,-1]   = Rho[:,-3]      - 2.0*Rho[:,-1]   + Rho[:,rrghost]
    diffu *= invDX*0.5
    
    # L1 norm of G 
    L1G = np.sum(DV*np.abs(G),axis=0)
    
    # Update micro macro unknwns
    for region in regions: # Solve per inner regions with appropriate boundary conditions
        zone = np.s_[:, region[2]:region[-3]+1]
        if isKinetic[region[2]]: # Use kinetic model
            (Rhon[zone],Gn[zone]) = solve_MM_Coupling(region,Rho,diffu,G,isKinetic,
                                                      MM,invDX,V,DV,dt, m2,epsilon,order)
        else: # Use fluid limit
            (Rhon[zone],Gn[zone]) = solve_Fluid_Coupling(region, Rho, diffu*invDX, m2, Nv, dt) 

    # Update F
    Fn = Rhon*MM + Gn
    
    # Computing second derivative at each cell center (wide stencil)
    diffu[:,0]    = Rho[:,llghost] - 2.0*Rho[:,0]    + Rho[:,2]
    diffu[:,1]    = Rho[:,lghost]  - 2.0*Rho[:,1]    + Rho[:,3]
    diffu[:,2:-2] = Rho[:,:-4]     - 2.0*Rho[:,2:-2] + Rho[:,4:]
    diffu[:,-2]   = Rho[:,-4]      - 2.0*Rho[:,-2]   + Rho[:,rghost]
    diffu[:,-1]   = Rho[:,-3]      - 2.0*Rho[:,-1]   + Rho[:,rrghost]
    diffu *= invDX*0.5
    
    # L1 norm of G 
    L1G = np.sum(DV*np.abs(G),axis=0)
    
    # Update the state of the cells 
    if Coupling:
        (isKineticn, regionsn) = updateState(diffu[0,:]*invDX[0,:]*0.5, L1G, isKinetic, regions, eta, delta, epsilon, m2, m4)
    else:
        isKineticn, regionsn = isKinetic, regions
    return (Rhon,Gn,Fn,isKineticn,regionsn)

def solve_Fluid_Coupling(region, Fluid, diffu, alpha, Nv, dt):
    """ Brief: Solves the diffusion limit with a diffusion coefficient alpha on a subdomain
    
     Input :
     - region: List containing indexes of the region
     - Fluid:  Rho_fluid at t^n                     (meshgrid form)
     - alpha:  Diffusion coefficient
     - Nv:     Number of cells in velocity
     - dt:     Time step
    
     Output :
     - Fluidn: updated fluit limit
     - Gn:     updated G
    """
    
    Nx = len(region)-4
    s  = np.s_[:,region[2]:region[-3]+1]
    
    Fluidn = np.zeros((Nv,Nx))   
    Fluidn = Fluid[s] + dt*alpha*diffu[s]
    Gn = np.zeros((Nv,Nx))
    
    return (Fluidn, Gn)

def solve_MM_Coupling(region, Rho, diffu, G, isKinetic, MM, invDX, V, DV, dt, m2, eps, order):
    """ Brief: Solves the Vlasov equation using the micro-macro model on a subdomain
    
    Input :
    - region:  List containing the region
    - Rho:     Rho at t^n                       (meshgrid form)
    - diffu:   Discrete 2nd derivative of rho   (meshgrid form)
    - G:       G   at t^n                       (meshgrid form)
    - MM:      Maxwellian                       (meshgrid form)
    - invDX:   One over cell length in position (meshgrid form)
    - V:       Cell centers in velocities       (meshgrid form)
    - DV:      Cell length in velocity          (meshgrid form)
    - m2:      2nd Maxwellian moment
    - eps:     Knudsen number
    
    Output :
    - G_new:   G   at t^n+1 (meshgrid form)
    - Rho_new: Rho at t^n+1 (meshgrid form)
    """
    
    Nv = G.shape[0]
    Nx = len(region)-4
    
    # Specific cells
    llghost = region[0]
    lghost  = region[1]
    first   = region[2]
    last    = region[-3]
    rghost  = region[-2]
    rrghost = region[-1]
    
    # Slices
    s   = np.s_[:, first:last+1] # Local slice
    im1 = np.s_[:,first:last-1]   # Local slice i-1
    ip1 = np.s_[:,first+2:last+1] # Local slice 1+1
    
    # Memory allocation
    gflux   = np.zeros((Nv,Nx+1))
    phi     = np.zeros((Nv,Nx))
    phiflux = np.zeros((Nv,Nx))
    rhoflux = np.zeros((Nv,Nx))
    tmp     = np.zeros((Nv,Nx))
    
    if order == 1 :
        if isKinetic[llghost]:
            G_llg = G[:,llghost]
        else:
            G_llg = -eps*V[0,llghost]*MM[0,llghost]*(Rho[:,lghost]-Rho[:,llghost-1])*0.5*invDX[0,llghost]
            
        if isKinetic[lghost]:
            G_lg  = G[:,lghost]
        else:
            G_lg  = -eps*V[0,lghost]*MM[0,lghost]*(Rho[:,llghost]-Rho[:,first])*0.5*invDX[0,lghost]
        if isKinetic[rghost]:
            G_rg  = G[:,rghost]
        else:
            G_rg  = -eps*V[0,rghost]*MM[0,rghost]*(Rho[:,rrghost]-Rho[:,last])*0.5*invDX[0,rghost]
        if isKinetic[rrghost]:
            G_rrg = G[:,rrghost]
        else:
            G_rrg = -eps*V[0,rrghost]*MM[0,rrghost]*(Rho[:,rghost]-Rho[:,(rrghost+1)%G.shape[1]])*0.5*invDX[0,rrghost]
    else:
        G_llg = G[:,llghost]
        G_lg  = G[:,lghost]
        G_rg  = G[:,rghost]
        G_rrg = G[:,rrghost]

    # ----- Computes G_new -----
    # Downstream flux on g
    gflux[:,0]    = G[:,first] - G_lg
    gflux[:,1:-1] = np.diff(G[s],axis=1)
    gflux[:,-1]   = G_rg - G[:,last]
    
    # Upwind flux
    phi = (V[s]>=0)*V[s]*gflux[:,:-1] + (V[s]<0)*V[s]*gflux[:,1:]
    phi *= invDX[s]
    
    # Centered difference on rho
    if Nx != 1:
        rhoflux[:,0]    = Rho[:,first+1] - Rho[:,lghost]
        rhoflux[:,1:-1] = Rho[ip1] - Rho[im1]
        rhoflux[:,-1]   = Rho[:,rghost]  - Rho[:,last-1]
    else:  # Case of a single cell region
        rhoflux[:,0]  = Rho[:,rghost] - Rho[:,lghost]
    rhoflux *= invDX[s]*0.5
    
    G_new = (eps*eps/dt)*G[s] - eps*phi + eps*MM[s]*np.tile(np.sum(DV[s]*phi, axis=0).T,(Nv,1)) - eps*rhoflux*V[s]*MM[s]
    G_new /= (1.0 + eps*eps/dt)
    
    # ----- Computes Rho_new -----   
    # Compute phi at first left and right ghostcells
    phi_lg = (V[:,lghost]>=0)*V[:,lghost]*(G_lg - G_llg) + (V[:,0]<0)*V[:,0]*gflux[:,0]
    phi_lg *= invDX[:,lghost]
    phi_rg = (V[:,-1]>=0)*V[:,-1]*gflux[:,-1] + (V[:,rghost]<0)*V[:,rghost]*(G_rrg - G_rg)
    phi_rg *= invDX[:,rghost]
    
    # Centered difference on phi
    if Nx != 1:
        phiflux[:,0]    = phi[:,1] - phi_lg
        phiflux[:,1:-1] = phi[:,2:] - phi[:,:-2]
        phiflux[:,-1]   = phi_rg - phi[:,-2]
    else: # Case of a single cell region
        phiflux[:,0]    = phi_rg - phi_lg
    
    # Centered difference on g
    if Nx != 1:
        tmp[:,0]    = G[:,first+1] - G_lg
        tmp[:,1:-1] = G[ip1] - G[im1]
        tmp[:,-1]   = G_rg  - G[:,last-1]
    else:  # Case of a single cell region
        tmp[:,0]  = G_rg - G_lg
      
    # term to integrate with respect to v
    tmp = V[s]*(eps/dt*tmp - phiflux)
    tmp = np.tile(np.transpose(np.sum(DV[s]*tmp, axis=0)),(Nv,1)) - m2*diffu[s]
    Rho_new = Rho[s] - dt/(eps*eps/dt+1)*0.5*invDX[s]*tmp
    
    return (Rho_new,G_new)

# def solve_Coupled(Rho, G, isKinetic, regions, eta, delta, 
#                   MM, invDX, V, DV, dt, m2, m4, epsilon,Coupling,order):
#     """ Brief: Solves the Vlasov equation at time t using a dynamic coupling method
    
#     Input :
#     - Rho:       Rho at t^n                       (meshgrid form)
#     - G:         G   at t^n                       (meshgrid form)
#     - isKinetic: Array of the current state of the cells (Boolean)
#     - regions:   List of list containing a given region indexes
#     - MM:        Maxwellian                       (meshgrid form)
#     - eta:       Threshold determining the change from fluid to kinetic
#     - delta:     Threshold determining the change from kinetic to fluid
#     - invDX:     One over cell length in position (meshgrid form)
#     - V:         Cell centers in velocities       (meshgrid form)
#     - DV:        Cell length in velocity          (meshgrid form)
#     - m2:        2nd Maxwellian moment
#     - eps:       Knudsen number
#     - Coupling:  Boolean that enables dynamic coupling
    
#     Output :
#     - G_new:      G   at t^n+1 (meshgrid form)
#     - Rho_new:    Rho at t^n+1 (meshgrid form)
#     - isKineticn: Updated state
#     - regionsn:   Updated regions
#     """
    
#     Nv = G.shape[0]
#     Nx = G.shape[1]
    
#     # Specific cells
#     llghost = -2
#     lghost  = -1
#     rghost  = 0
#     rrghost = 1
    
#     # Memory allocation
#     Rhon  = np.zeros((Nv,Nx))
#     Gn    = np.zeros((Nv,Nx))
#     Fn    = np.zeros((Nv,Nx))
#     diffu = np.zeros((Nv,Nx))
    
#     # Computing second derivative at each cell center (wide stencil)
#     diffu[:,0]    = Rho[:,llghost] - 2.0*Rho[:,0]    + Rho[:,2]
#     diffu[:,1]    = Rho[:,lghost]  - 2.0*Rho[:,1]    + Rho[:,3]
#     diffu[:,2:-2] = Rho[:,:-4]     - 2.0*Rho[:,2:-2] + Rho[:,4:]
#     diffu[:,-2]   = Rho[:,-4]      - 2.0*Rho[:,-2]   + Rho[:,rghost]
#     diffu[:,-1]   = Rho[:,-3]      - 2.0*Rho[:,-1]   + Rho[:,rrghost]
#     diffu *= invDX*0.5
    
#     # L1 norm of G 
#     L1G = np.sum(DV*np.abs(G),axis=0)
    
#     # Update micro macro unknwns
#     for region in regions: # Solve per inner regions with appropriate boundary conditions
#         zone = np.s_[:, region[2]:region[-3]+1]
#         if isKinetic[region[2]]: # Use kinetic model
#             (Rhon[zone],Gn[zone]) = solve_MM_Coupling(region,Rho,diffu,G,isKinetic,
#                                                       MM,invDX,V,DV,dt, m2,epsilon,order)
#         else: # Use fluid limit
#             (Rhon[zone],Gn[zone]) = solve_Fluid_Coupling(region, Rho, diffu*invDX, m2, Nv, dt) 
#             Gn[zone] = reconstruct(region, Rho, epsilon, MM, V, invDX, m2, order)

#     # Update F
#     Fn = Rhon*MM + Gn
    
#     # Computing second derivative at each cell center (wide stencil)
#     diffu[:,0]    = Rho[:,llghost] - 2.0*Rho[:,0]    + Rho[:,2]
#     diffu[:,1]    = Rho[:,lghost]  - 2.0*Rho[:,1]    + Rho[:,3]
#     diffu[:,2:-2] = Rho[:,:-4]     - 2.0*Rho[:,2:-2] + Rho[:,4:]
#     diffu[:,-2]   = Rho[:,-4]      - 2.0*Rho[:,-2]   + Rho[:,rghost]
#     diffu[:,-1]   = Rho[:,-3]      - 2.0*Rho[:,-1]   + Rho[:,rrghost]
#     diffu *= invDX*0.5
    
#     # L1 norm of G 
#     L1G = np.sum(DV*np.abs(G),axis=0)
    
#     # Update the state of the cells 
#     if Coupling:
#         (isKineticn, regionsn) = updateState(diffu[0,:]*invDX[0,:]*0.5, L1G, isKinetic, regions, eta, delta, epsilon, m2, m4)
#     else:
#         isKineticn, regionsn = isKinetic, regions
#     return (Rhon,Gn,Fn,isKineticn,regionsn)

# def Calc_dx(val, steps):
#     Nv = val.shape[0]
#     Nx = val.shape[1]
#     res = np.zeros((Nv,Nx))
#     res[:,0]    = val[:,1] - val[:,-1]
#     res[:,1:-1] = val[:,2:] - val[:,:-2]
#     res[:,-1]   = val[:,0]  - val[:,-2]
#     res*=0.5*steps
#     return res

# def reconstruct(region, Rho, eps, MM, V, invDX, m2, order):

#     Nv = invDX.shape[0]
#     Nx = len(region)-4
    
#     # Specific cells
#     llg   = region[0]
#     lg    = region[1]
#     first = region[2]
#     last  = region[-3]
#     rg    = region[-2]
#     rrg   = region[-1]
    
#     dxrho   = Calc_dx(Rho, invDX)
#     dxxrho  = Calc_dx(dxrho, invDX)
#     dxxxrho = Calc_dx(dxxrho, invDX)
    
#     # Slices
#     s   = np.s_[:, first:last+1] # Local slice

#     G = np.zeros((Nv,Nx))
#     if order==1:     
#         G += -eps*invDX[s]*V[s]*MM[s]*dxrho[s]
#     elif order==2:
#         G += -eps*invDX[s]*V[s]*MM[s]*dxrho[s]
#         G += eps*eps*MM[s]*(-m2+V[s]*V[s])*dxxrho[s]
#     elif order == 3:
#         G -= eps*V[s]*MM[s]*dxrho[s]
#         G += eps*eps*MM[s]*(-m2+V[s]*V[s])*dxxrho[s]
#         G += eps*eps*eps*MM[s]*(2*m2*V[s]-V[s]*V[s]*V[s])*dxxxrho[s]




# def solve_Fluid_Coupling(region, Fluid, diffu, alpha, Nv, dt):
#     """ Brief: Solves the diffusion limit with a diffusion coefficient alpha on a subdomain
    
#      Input :
#      - region: List containing indexes of the region
#      - Fluid:  Rho_fluid at t^n                     (meshgrid form)
#      - alpha:  Diffusion coefficient
#      - Nv:     Number of cells in velocity
#      - dt:     Time step
    
#      Output :
#      - Fluidn: updated fluit limit
#      - Gn:     updated G
#     """
    
#     Nx = len(region)-4
#     s  = np.s_[:,region[2]:region[-3]+1]
    
#     Fluidn = np.zeros((Nv,Nx))   
#     Fluidn = Fluid[s] + dt*alpha*diffu[s]
#     Gn = np.zeros((Nv,Nx))
    
#     return (Fluidn, Gn)

# def solve_MM_Coupling(region, Rho, diffu, G, isKinetic, MM, invDX, V, DV, dt, m2, eps, order):
#     """ Brief: Solves the Vlasov equation using the micro-macro model on a subdomain
    
#     Input :
#     - region:  List containing the region
#     - Rho:     Rho at t^n                       (meshgrid form)
#     - diffu:   Discrete 2nd derivative of rho   (meshgrid form)
#     - G:       G   at t^n                       (meshgrid form)
#     - MM:      Maxwellian                       (meshgrid form)
#     - invDX:   One over cell length in position (meshgrid form)
#     - V:       Cell centers in velocities       (meshgrid form)
#     - DV:      Cell length in velocity          (meshgrid form)
#     - m2:      2nd Maxwellian moment
#     - eps:     Knudsen number
    
#     Output :
#     - G_new:   G   at t^n+1 (meshgrid form)
#     - Rho_new: Rho at t^n+1 (meshgrid form)
#     """
    
#     Nv = G.shape[0]
#     Nx = len(region)-4
    
#     # Specific cells
#     llghost = region[0]
#     lghost  = region[1]
#     first   = region[2]
#     last    = region[-3]
#     rghost  = region[-2]
#     rrghost = region[-1]
    
#     # Slices
#     s   = np.s_[:, first:last+1] # Local slice
#     im1 = np.s_[:,first:last-1]   # Local slice i-1
#     ip1 = np.s_[:,first+2:last+1] # Local slice 1+1
    
#     # Memory allocation
#     gflux   = np.zeros((Nv,Nx+1))
#     phi     = np.zeros((Nv,Nx))
#     phiflux = np.zeros((Nv,Nx))
#     rhoflux = np.zeros((Nv,Nx))
#     tmp     = np.zeros((Nv,Nx))

#     # ----- Computes G_new -----
#     # Downstream flux on g
#     gflux[:,0]    = G[:,first] - G[:,lghost]
#     gflux[:,1:-1] = np.diff(G[s],axis=1)
#     gflux[:,-1]   = G[:,rghost] - G[:,last]
    
#     # Upwind flux
#     phi = (V[s]>=0)*V[s]*gflux[:,:-1] + (V[s]<0)*V[s]*gflux[:,1:]
#     phi *= invDX[s]
    
#     # Centered difference on rho
#     if Nx != 1:
#         rhoflux[:,0]    = Rho[:,first+1] - Rho[:,lghost]
#         rhoflux[:,1:-1] = Rho[ip1] - Rho[im1]
#         rhoflux[:,-1]   = Rho[:,rghost]  - Rho[:,last-1]
#     else:  # Case of a single cell region
#         rhoflux[:,0]  = Rho[:,rghost] - Rho[:,lghost]
#     rhoflux *= invDX[s]*0.5
    
#     G_new = (eps*eps/dt)*G[s] - eps*phi + eps*MM[s]*np.tile(np.sum(DV[s]*phi, axis=0).T,(Nv,1)) - eps*rhoflux*V[s]*MM[s]
#     G_new /= (1.0 + eps*eps/dt)
    
#     # ----- Computes Rho_new -----   
#     # Compute phi at first left and right ghostcells
#     phi_lg = (V[:,lghost]>=0)*V[:,lghost]*(G[:,lghost] - G[:,llghost]) + (V[:,0]<0)*V[:,0]*gflux[:,0]
#     phi_lg *= invDX[:,lghost]
#     phi_rg = (V[:,-1]>=0)*V[:,-1]*gflux[:,-1] + (V[:,rghost]<0)*V[:,rghost]*(G[:,rrghost] - G[:,rghost])
#     phi_rg *= invDX[:,rghost]
    
#     # Centered difference on phi
#     if Nx != 1:
#         phiflux[:,0]    = phi[:,1] - phi_lg
#         phiflux[:,1:-1] = phi[:,2:] - phi[:,:-2]
#         phiflux[:,-1]   = phi_rg - phi[:,-2]
#     else: # Case of a single cell region
#         phiflux[:,0]    = phi_rg - phi_lg
    
#     # Centered difference on g
#     if Nx != 1:
#         tmp[:,0]    = G[:,first+1] - G[:,lghost]
#         tmp[:,1:-1] = G[ip1] - G[im1]
#         tmp[:,-1]   = G[:,rghost]  - G[:,last-1]
#     else:  # Case of a single cell region
#         tmp[:,0]  = G[:,rghost] - G[:,lghost]
      
#     # term to integrate with respect to v
#     tmp = V[s]*(eps/dt*tmp - phiflux)
#     tmp = np.tile(np.transpose(np.sum(DV[s]*tmp, axis=0)),(Nv,1)) - m2*diffu[s]
#     Rho_new = Rho[s] - dt/(eps*eps/dt+1)*0.5*invDX[s]*tmp
    
#     return (Rho_new,G_new)
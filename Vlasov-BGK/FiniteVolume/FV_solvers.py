""" Brief : Module containing usefull functions and Finite Volume solvers for kinetic, fluid and hybrid method
    Author : Laidin Tino, Université de Nantes
    Date : July 2021
"""

from FV_tools import *

##################################################################
#                      NON COUPLED SOLVERS                       #
##################################################################
def CenteredFlux(F):
    """ Brief: Centered flux with periodic BC
    
    Input :
    - F:   F at t^n                         (meshgrid form)
    
    Output :
    - FluxPos: Centered flux in position    (meshgrid form)
    """
    
    Nv = F.shape[0]
    Nx = F.shape[1]
    
    # Ghost cells
    lg  = -1
    rg  = 0
    
    # Memory allocation
    FluxPos   = np.zeros((Nv,Nx+1))

    # Centered flux
    FluxPos[:,0]    = 0.5*(F[:,lg]+F[:,0])
    FluxPos[:,1:-1] = 0.5*(F[:,:-1]+F[:,1:])
    FluxPos[:,-1]   = 0.5*(F[:,-1]+F[:,rg])
    
    return FluxPos

def dxx(val, steps):
    """" Brief: Compute second derivative with wide stencil 
    
    Input:
    - val
    - steps:
    
    Output:
    - res:
    """
    
    llg = -2
    lg  = -1
    rg  = 0
    rrg = 1
    res = np.zeros(val.shape[0])
    
    res[0]    = steps[1]*(val[2]-val[0])  - steps[lg]*(val[0]-val[llg])
    res[1]    = steps[2]*(val[3]-val[1])  - steps[0]*(val[1]-val[lg])
    res[2:-2] = steps[3:-1]*(val[4:]-val[2:-2])  - steps[1:-3]*(val[2:-2]-val[:-4])
    res[-2]   = steps[-1]*(val[rg]-val[-2])  - steps[-3]*(val[-2]-val[-4])
    res[-1]   = steps[rg]*(val[rrg]-val[-1]) - steps[-2]*(val[-1]-val[-3])
    res *= 0.5*steps
    
    return res

def solve_Kinetic(F, MM, invDX, V, DV, invDV, dt, m2, inveps):
    """ Brief: Solves the Vlasov equation on the whole domain
    
    Input :
    - F:      F at t^n                         (meshgrid form)
    - MM:     Maxwellian                       (meshgrid form)
    - invDX:  One over cell length in position (meshgrid form)
    - V:      Cell centers in velocities       (meshgrid form)
    - invDV:  One over cell length in velocity (meshgrid form)
    - dt:     Time step
    - inveps: Knudsen number
    
    Output :
    - F:     F   at t^n+1                      (meshgrid form)
    """
    
    Nv = F.shape[0]
    Nx = F.shape[1]
    
    # Memory allocation
    FluxPos = CenteredFlux(F)
    # Transport
    Fn = F - dt*inveps*invDX*(FluxPos[:,1:]-FluxPos[:,:-1])*V
    # Stiff term
    Fn += dt*inveps*inveps*(np.tile(np.transpose(np.sum(DV*F, axis=0)),(Nv,1))*MM - F)
    
    return Fn

def solve_Fluid(rho, alpha, invdx, dt):
    """ Brief: Solves the limit scheme on the whole domain
    
     Input :
     - rho:   rho at t^n
     - alpha: Diffusion coefficient
     - invdx: Inverse of the space discretization
     - dt:    Time step
    
     Output :
     - rhon:  Updated fluid limit
    """
    
    Nx = rho.shape[0]
    # Ghost cells
    llg = -2
    lg  = -1
    rg  = 0
    rrg = 1
    
    # Memory allocation
    diffu = np.zeros(Nx)
    
    diffu[0]    = invdx[1]*(rho[2]-rho[0])  - invdx[lg]*(rho[0]-rho[llg])
    diffu[1]    = invdx[2]*(rho[3]-rho[1])  - invdx[0]*(rho[1]-rho[lg])
    diffu[2:-2] = invdx[3:-1]*(rho[4:]-rho[2:-2])  - invdx[1:-3]*(rho[2:-2]-rho[:-4])
    diffu[-2]   = invdx[-1]*(rho[rg]-rho[-2])  - invdx[-3]*(rho[-2]-rho[-4])
    diffu[-1]   = invdx[rg]*(rho[rrg]-rho[-1]) - invdx[-2]*(rho[-1]-rho[-3])

    rhon = rho + dt*alpha*0.25*invdx*diffu
    return rhon

def solve_FluidH(rho, m2, m4, invdx, dt, eps):
    """ Brief: Solves the higher order diffusion limit (large stencil) on the whole domain
    
     Input :
     - rho:    rho at t^n
     - m2, m4: 2nd and 4th discrete moments
     - invdx:  Inverse of the space discretization
     - dt:     Time step
     - eps:    Knudsen number
    
     Output :
     - rhon:   Updated higher order fluid limit
    """
    
    Nx = fluid.shape[0]
    # Ghost cells
    llg = -2
    lg  = -1
    rg  = 0
    rrg = 1
    
    # Memory allocation
    dxxrho = np.zeros(Nx)
    tmpbis = np.zeros(Nx)
    
    dxxrho[0]    = invdx[0]*(invdx[1]*(rho[2]-rho[0])  - invdx[lg]*(rho[0]-rho[llg]))
    dxxrho[1]    = invdx[1]*(invdx[2]*(rho[3]-rho[1])  - invdx[0]*(rho[1]-rho[lg]))
    dxxrho[2:-2] = invdx[2:-2]*(invdx[3:-1]*(rho[4:]-rho[2:-2])  - invdx[1:-3]*(rho[2:-2]-rho[:-4]))
    dxxrho[-2]   = invdx[-2]*(invdx[-1]*(rho[rg]-rho[-2])  - invdx[-3]*(rho[-2]-rho[-4]))
    dxxrho[-1]   = invdx[-1]*(invdx[rg]*(rho[rrg]-rho[-1]) - invdx[-2]*(rho[-1]-rho[-3]))
    dxxrho *=0.5
    
    tmp = m2*rho + eps*eps*(2*m2*m2-m4)*dxxrho
    
    tmpbis[0]    = 0.5*invdx[0]*(invdx[1]*(tmp[2]-tmp[0])  - invdx[lg]*(tmp[0]-tmp[llg]))
    tmpbis[1]    = 0.5*invdx[1]*(invdx[2]*(tmp[3]-tmp[1])  - invdx[0]*(tmp[1]-tmp[lg]))
    tmpbis[2:-2] = 0.5*invdx[2:-2]*(invdx[3:-1]*(tmp[4:]-tmp[2:-2])  - invdx[1:-3]*(tmp[2:-2]-tmp[:-4]))
    tmpbis[-2]   = 0.5*invdx[-2]*(invdx[-1]*(tmp[rg]-tmp[-2])  - invdx[-3]*(tmp[-2]-tmp[-4]))
    tmpbis[-1]   = 0.5*invdx[-1]*(invdx[rg]*(tmp[rrg]-tmp[-1]) - invdx[-2]*(tmp[-1]-tmp[-3]))
    
    rhon = rho + dt*0.5*tmpbis
    return rhon

##################################################################
#                      DYNAMIC COUPLING                          #
################################################################## 

#  --------------- COUPLING TOOLS --------------------
def indicators(rho_kin, rho_fluid, invdx, m2, m4):
    
    llghost = -2
    lghost  = -1
    rghost  = 0
    rrghost = 1
    
    indKinetic = np.zeros(invdx.shape[0])
    indFluid = np.zeros(invdx.shape[0])
    
    indKinetic[0]    = rho_kin[llghost] - 2.0*rho_kin[0]    + rho_kin[2]
    indKinetic[1]    = rho_kin[lghost]  - 2.0*rho_kin[1]    + rho_kin[3]
    indKinetic[2:-2] = rho_kin[:-4]     - 2.0*rho_kin[2:-2] + rho_kin[4:]
    indKinetic[-2]   = rho_kin[-4]      - 2.0*rho_kin[-2]   + rho_kin[rghost]
    indKinetic[-1]   = rho_kin[-3]      - 2.0*rho_kin[-1]   + rho_kin[rrghost]
    indKinetic *= invdx*0.25*(2.*m2*m2-m4)
    
    indFluid[0]    = rho_fluid[llghost] - 2.0*rho_fluid[0]    + rho_fluid[2]
    indFluid[1]    = rho_fluid[lghost]  - 2.0*rho_fluid[1]    + rho_fluid[3]
    indFluid[2:-2] = rho_fluid[:-4]     - 2.0*rho_fluid[2:-2] + rho_fluid[4:]
    indFluid[-2]   = rho_fluid[-4]      - 2.0*rho_fluid[-2]   + rho_fluid[rghost]
    indFluid[-1]   = rho_fluid[-3]      - 2.0*rho_fluid[-1]   + rho_fluid[rrghost]
    indFluid *= invdx*0.25*(2.*m2*m2-m4)
    
    return (indKinetic, indFluid)

def updateState(dxxrho, L1G, isKinetic, eta, delta, eps, m2, m4):
    """ Brief: Computes the new kinetic and fluid cells of the domain based on their current state
    
    Input:
    - dxxrho:    Discrete 2nd derivative of rho at t^n
    - L1G:       L1 norm of  G at t^n : ||g(t^n, x_i+.5, . )||_L1
    - isKinetic: Array of boolean, each element corresponds to the state of a cell
    - eta:       Threshold determining the change from fluid to kinetic
    - delta:     Threshold determining the change from kinetic to fluid
    - eps:       Knudsen number
    - m2, m4:    2nd and 4th moment of the maxwellian
    
    Output:
    - new_state: Boolean list of updated cells' state
    """
    
    Nx = dxxrho.shape[0]
    toFluid   = np.full(Nx,True, dtype=bool)
    toKinetic = np.full(Nx,True, dtype=bool)
     
    toFluid = toFluid & (L1G  < delta)
    toFluid = toFluid & (abs((2.*m2*m2+m4)*dxxrho) <= eta)   # Stays fluid criterion
    
    toKinetic = abs((2*m2*m2+m4)*dxxrho)>eta
    
    # If Kinetic and tofluid, flip ; If Fluid and toKinetic, flip
    newState = np.where( (isKinetic & toFluid) | ( ~isKinetic & toKinetic), ~isKinetic, isKinetic)
    
    # Storing regions with two ghost cells on each side
    regions = [[-2,-1,0]] # left ghostcell by periodicity and first cell
    prev = newState[0]
    count = 0
    for i in range(1,Nx-1):
        curr = newState[i]
        if curr == prev:
            regions[count].append(i)
        else:
            if i != Nx-2:
                regions[count].extend([i,i+1])  # Right ghostcells of current region
            else:
                regions[count].extend([i+1,-1])   # Right ghostcells of current region
            count += 1                            # Pass to the next region
            regions.append([i-2,i-1, i])          # First cell of next region and left ghostcells
        prev = curr
        
    # Dealing with the last cell 
    curr = newState[Nx-1]
    if  curr == prev:
        regions[count].extend([Nx-1,0,1]) # last cell and right ghostcells by periodicity
    else:
        regions[count].extend([Nx-1, 0]) # Right ghostcells of the second to last region
        regions.append([Nx-3, Nx-2, Nx-1, 0, 1]) # Last region is the single last cell
    
    return (newState, regions)

def CenteredFlux_Coupling(region, F):
    """ Brief: Centered flux in a coupling setting
    
    Input :
    - region: Kinetic subdomain
    - F:      F at t^n                        (meshgrid form)
    
    Output :
    - FluxPos:  centered flux in position     (meshgrid form)
    """
    
    Nv = F.shape[0]
    Nx = len(region)-4
    
    # Specific cells
    lg    = region[1]
    first = region[2]
    last  = region[-3]
    rg    = region[-2]
    
    # Slices
    i   = np.s_[:,first:last]   # Local slice i
    ip1 = np.s_[:,first+1:last+1] # Local slice 1+1
    
    # Memory allocation
    FluxPos   = np.zeros((Nv,Nx+1))
    
    # Centered flux
    FluxPos[:,0]    = 0.5*(F[:,lg]+F[:,first])
    FluxPos[:,1:-1] = 0.5*(F[i]+F[ip1])
    FluxPos[:,-1]   = 0.5*(F[:,last]+F[:,rg])
    
    return FluxPos

def ProjToFluid(Fn_loc, DV_loc):
    """"Brief: Project the kinetic solution onto the fluid scale
    
     Input :
     - Fn_loc: Local kinetic solution
     - DV_loc: Local velocity steps

     Output :
     - proj : projected solution
    """
    
   
    proj = np.sum(Fn_loc*DV_loc, axis=0)
    
     # Add trapezoidal rule ?
        
    return proj

def liftToKin(region, rho, dxrho, dxxxrho, V, MM, invDX, eps, m2, order):
    """"Brief: Lifts the density rho to the distribution function f using the Chapmann Enskog expansion
    
     Input :
     - region: Fluid subdomain on which to lift the fluid solution
     - rho:    rho at t^n
     - V:      Cell centers in velocities       (meshgrid form)
     - MM:     Maxwellian                       (meshgrid form)
     - invDX:  One over cell length in position (meshgrid form)
     - eps:    Knudsen number
     - m2:     2nd discrete moment of the maxwellian 
     - order:  Order considered in the C-E expansion

     Output :
     - F_lifted : lifted distribution function in the region
    """
    
    Nv = invDX.shape[0]
    Nx = len(region)-4
    
    # Specific cells
    llg   = region[0]
    lg    = region[1]
    first = region[2]
    last  = region[-3]
    rg    = region[-2]
    rrg   = region[-1]
    
    # Slices
    s   = np.s_[:, first:last+1] # Local slice
    
    F_lifted = np.tile(rho[first:last+1].T,(Nv,1))*MM[s]
    if order==1:     
        F_lifted += -eps*invDX[s]*V[s]*MM[s]*np.tile(dxrho[first:last+1].T,(Nv,1))
    elif order==2:
        dxxrho = dxx(rho, invDX[0,:])
        F_lifted += -eps*invDX[s]*V[s]*MM[s]*np.tile(dxrho[first:last+1].T,(Nv,1))
        F_lifted += eps*eps*MM[s]*(-m2+V[s]*V[s])*np.tile(dxxrho[first:last+1].T,(Nv,1))
    elif order == 3:
        dxxrho = dxx(rho, invDX[0,:])
        F_lifted -= eps*V[s]*MM[s]*np.tile(dxrho[first:last+1].T,(Nv,1))
        F_lifted += eps*eps*MM[s]*(-m2+V[s]*V[s])*np.tile(dxxrho[first:last+1].T,(Nv,1))
        F_lifted += eps*eps*eps*MM[s]*(2*m2*V[s]-V[s]*V[s]*V[s])*np.tile(dxxxrho[first:last+1].T,(Nv,1))
    
    return F_lifted

def dxx(val, steps):
    """"Brief: Compute the discrete second derivative with wide stencil on the whole domain
    
     Input :
     - val:   input array
     - steps: discretization steps

     Output :
     - res : output array
    """
    
    Nx = val.shape[0]
    # Specific cells
    llg = -2
    lg  = -1
    rg  = 0
    rrg = 1
    res = np.zeros(Nx)
    res[0]    = steps[1]*(val[2]-val[0])  - steps[lg]*(val[0]-val[llg])
    res[1]    = steps[2]*(val[3]-val[1])  - steps[0]*(val[1]-val[lg])
    res[2:-2] = steps[3:-1]*(val[4:]-val[2:-2])  - steps[1:-3]*(val[2:-2]-val[:-4])
    res[-2]   = steps[-1]*(val[rg]-val[-2])  - steps[-3]*(val[-2]-val[-4])
    res[-1]   = steps[rg]*(val[rrg]-val[-1]) - steps[-2]*(val[-1]-val[-3])
    res *= 0.25*steps
    return res

#  ------------ ADAPTED LOCAL SOLVERS ----------------

def solve_Kinetic_Coupling(region, F, isKinetic, MM, invDX, V, DV, dt, m2, eps, inveps):
    """ Brief: Solves the Vlasov equation in a coupling setting
    
    Input :
    - region: Kinetic subdomain
    - F:      F at t^n                         (meshgrid form)
    - rho:    rho at t^n
    - MM:     Maxwellian                       (meshgrid form)
    - invDX:  One over cell length in position (meshgrid form)
    - V:      Cell centers in velocities       (meshgrid form)
    - invDV:  One over cell length in velocity (meshgrid form)
    - dt:     Time step
    - inveps: Knudsen number
    
    Output :
    - F:     F   at t^n+1                      (meshgrid form)
    """
    
    Nv = F.shape[0]
    Nx = len(region)-4
    # Slice
    s   = np.s_[:, region[2]: region[-3]+1]  # Local slice
    # Numerical flux
    FluxPos = CenteredFlux_Coupling(region, F)
    # Transport
    Fn = F[s] - dt*inveps*invDX[s]*(FluxPos[:,1:]-FluxPos[:,:-1])*V[s]
    # Stiff term
    Fn += dt*inveps*inveps*(np.tile(np.sum(DV[s]*F[s], axis=0).T,(Nv,1))*MM[s] - F[s])

    return Fn

def solve_Fluid_Coupling(region, rho, diffu, alpha, Nv, invdx, dt):
    """ Brief: Solves the limit scheme in a coupling setting
    
     Input :
     - region: Fluid subdomain
     - rho:    rho at t^n
     - alpha:  Diffusion coefficient
     - invdx:  Inverse of the space steps
     - dt:     Time step
    
     Output :
     - rhon:  Updated fluid limit
    """

    s   = np.s_[region[2]:region[-3]+1] # Local slice
    rhon = rho[s] + dt*alpha*0.5*invdx[s]*diffu[s]
    
    return rhon

#  ------------ GLOBAL COUPLED SOLVER ----------------

def solve_Coupled(rho, F, dxxrho, isKinetic, regions, eta, delta,
                  MM, invDX, V, DV, dt, m2, m4, epsilon, inveps, Coupling, order, i):
    """ Brief: Solves the Vlasov equation at time t using a dynamic coupling method
    
    Input :
    - F:         F   at t^n                       (meshgrid form)
    - rho:       rho at t^n                       
    - isKinetic: Array of the current state of the cells (Boolean)
    - regions:   List of list containing a given region indexes
    - MM:        Maxwellian                       (meshgrid form)
    - eta:       Threshold determining the change from fluid to kinetic
    - delta:     Threshold determining the change from kinetic to fluid
    - invDX:     One over cell length in position (meshgrid form)
    - V:         Cell centers in velocities       (meshgrid form)
    - DV:        Cell length in velocity          (meshgrid form)
    - m2,m4:     2nd and 4th Maxwellian moments
    - eps:       Knudsen number
    - inveps:    Inverse of the Knudsen number
    - Coupling:  Boolean that enables dynamic coupling
    
    Output :
    - Fn:         F at t^n+1
    - rhon:       rho at t^n+1
    - isKineticn: Updated state
    - regionsn:   Updated regions
    """
    
    Nv = F.shape[0]
    Nx = F.shape[1]
    
    # Memory allocation
    rhon    = np.zeros(Nx)
    Fn      = np.zeros((Nv,Nx))
    dxrho   = np.zeros(Nx)
    dxxxrho = np.zeros(Nx)
    
    # Update micro macro unknwns
    for region in regions: # Solve per inner regions with appropriate boundary conditions
        # Slices
        s_phase = np.s_[:, region[2]:region[-3]+1]
        s_space = np.s_[region[2]:region[-3]+1]
        if isKinetic[region[2]]: # Use kinetic model
            Fn[s_phase]   = solve_Kinetic_Coupling(region, F, isKinetic, MM, invDX, V, DV, dt, m2, epsilon, inveps)
            # Project Kinetic to fluid
            rhon[s_space] = ProjToFluid(Fn[s_phase], DV[s_phase])
        else: # Use fluid limit
            rhon[s_space] = solve_Fluid_Coupling(region, rho, dxxrho, m2, Nv, invDX[0,:], dt)
            # Lift fluid to Kinetic
            Fn[s_phase]   = liftToKin(region, rho, dxrho, dxxxrho, V, MM, invDX, epsilon, m2, order)
            
    # First derivative of rho
    dxrho[0]    = rho[1] - rho[-1]
    dxrho[1:-1] = rho[2:] - rho[0:-2]
    dxrho[-1]   = rho[0]  - rho[-2]
    dxrho*=0.5*invDX[0,:]
    # Second derivative of rho
    dxxrho = dxx(rhon, invDX[0,:])
    # Third derivative of rho
    dxxxrho[0]    = dxxrho[1] - dxxrho[-1]
    dxxxrho[1:-1] = dxxrho[2:] - dxxrho[:-2]
    dxxxrho[-1]   = dxxrho[0]  - dxxrho[-2]
    dxxxrho*=0.5*invDX[0,:]   
    
    # Computing f and rho on the whole domain by projection and lifting
    for region in regions:
        # Slices
        s_phase = np.s_[:, region[2]:region[-3]+1]
        s_space = np.s_[region[2]:region[-3]+1]
        if isKinetic[region[2]]: # Project Kinetic to fluid
            rhon[s_space] = ProjToFluid(Fn[s_phase], DV[s_phase])
        else: # Lift fluid to Kinetic
            Fn[s_phase]   = liftToKin(region, rho, dxrho, dxxxrho, V, MM, invDX, epsilon, m2, order)
            
    L1G = np.sum(DV*np.abs(Fn - np.tile(rhon.T,(Nv,1))*MM))
    
    # Update the state of the cells 
    if Coupling:
        isKineticn, regionsn = updateState(dxxrho, L1G, isKinetic, eta, delta, epsilon, m2, m4)
    else:
        isKineticn, regionsn = isKinetic, regions
    
    return (rhon, Fn, dxxrho, isKineticn, regionsn)
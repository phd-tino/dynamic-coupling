""" Brief : Module containing usefull discretization and post-treatment functions
    Author : Laidin Tino, Université de Nantes
    Date : July 2021
"""

##################################################################
#                            TOOLS                               #
##################################################################

import numpy as np
import scipy as sp
import scipy.sparse
import scipy.sparse.linalg
import matplotlib.pyplot as plt
import time

# ------------------------------------------------------------- #
         ##### DISCRETIZATION OF THE PHASE SPACE #####
# ------------------------------------------------------------- #
def discretizeV(L,vstar):
    """ Brief: Creates a discretization of the symmetric velocity domain [-vstar, vstar] with 2L cells.
    
    Input: 
    - vstar: Right endpoint
    - L:     Number of cells with positive velocities
    
    Output: 
    - v:     Cell centers
    - vhalf: Cell interfaces
    - dv:    Cell lengths
    - Nv:    Number of cells
    - vmin:  Left endpoint
    - vmax:  Right endpoint
    """
    
    vmax = vstar
    vmin = -vstar
    Nv = 2 * L
    vhalf = np.linspace(vmin,vmax, Nv+1) # Cell interface points
    dv = np.diff(vhalf) # Cell length
    v = vhalf[:len(vhalf)-1] + dv*.5 # Cell centers
    return (v,vhalf,dv,Nv,vmin,vmax)

def discretizeX(Nx,X):
    """ Brief: Creates a discretization of the periodic space domain of length X with Nx cells. Warning: if Nx is even it is changed to Nx+1.
    
    Input: 
    - X:  Length of the domain
    - Nx: Number of cells
    
    Output: 
    - x:     Cell centers
    - xhalf: Cell interfaces
    - dx:    Cell length
    - Nx:    Number of cells
    - xmin:  left endpoint
    - xmax:  right endpoint
    """
    
    if Nx % 2 == 0:
        Nx = Nx + 1

    xmin = 0
    xmax = X

    xhalf = np.linspace(xmin,xmax, Nx + 1) # Cell interface points
    dx = np.diff(xhalf) # Cell lengths
    invdx = 1.0/dx
    xhalf = xhalf[:len(xhalf)-1] # Interface points (last interface forgotten by periodicity)
    x = xhalf + dx*0.5  # Cell centers
    return (x,xhalf,dx,invdx,Nx,xmin,xmax)

def discretizeXV(x,Nx,dx,invdx,v,Nv,dv):
    """ Brief: Puts the discretization of the phase space in meshgrid (matrix) form. Rows are velocities and columns are positions.
    
    Input: 
    - x:     Cell centers in position
    - Nx:    Number of cells in position
    - dx:    Lengths of cells in position
    - invdx: One over lengths of cells in position
    - v:     Cell centers in position
    - Nv:    Number of cells in velocities
    - dv:    Lengths of cells in velocities
    
    Output: 
    - X:     Cell centers in position              (meshgrid form)
    - DX:    Lengths of cells in position          (meshgrid form)
    - invDX: One over lengths of cells in position (meshgrid form)
    - V:     Cell centers in position              (meshgrid form)
    - DV:    Lengths of cells in velocities        (meshgrid form)
    - N:     Number of cells in phase space
    """
    
    [X,V] = np.meshgrid(x,v)
    DV = np.transpose(np.tile(dv,(Nx,1)))
    DX = np.tile(dx,(Nv,1))
    invDX = 1.0 / DX
    invDV = 1.0 / DV
    N = Nx * Nv
    return (X,V,DV,invDV,DX,invDX,N)

# ------------------------------------------------------------- #
                ##### DISCRETE MAXWELLIANS #####
# ------------------------------------------------------------- #
def gaussian(v):
    """ Brief: Gaussian distribution """
    return np.exp(-v*v*0.5) / np.sqrt(2.0 * np.pi)

def one(v):
    """ Brief: cst """
    return np.ones(len(v))

def Maxwellian(dist, v, vhalf, dv, Nx):
    """ Brief : Computes the Maxwellian distribution from the velocity discretization.
    
    Input : 
    - dist:  distribution function
    - v:     cell centers
    - vhalf: interface points
    - dv:    cell length
    - Nx:    number of cells in x 
    
    Output:
    - M: Maxwellian at cell center 
    (- Mhalf: Maxwellian at interfaces)
    - MM: Maxwellian at cell center (meshgrid form)
    """
        
    #BGK
    M = dist(v) # In cells
    MassM = np.sum(M * dv)
    M = M / MassM # Normalize mass
    MM = np.transpose(np.tile(M,(Nx,1)))
    Mhalf = [] # Interfaces are not needed
   
    return (M,Mhalf,MM)

def Maxw_moment(M,i,v,dv):
    """ Brief: Cumputes the i-th moment of the Maxwellian """
    return np.sum(v**i*M*dv)

# ------------------------------------------------------------- #
                    ###### INITIAL DATA ######
# ------------------------------------------------------------- #
def init_Gaussian(X,V,xstar):
    """ Brief: Centered Gaussian """
    F = gaussian(V)*gaussian((X-np.pi/2)/0.3)
    return F

def init_Oscillation(X,V,xstar):
    """ Brief: Perturbed Gaussian """
    F = gaussian(V)* (1. + 1.*np.cos(2 * np.pi * X / xstar))
    return F

def init_ball(X,V,xstar,vstar):
    """ Brief: indicator function of an ellipse """
    F = 1.*((X-0.5*xstar)**2+(V/2/vstar)**2 < 0.2**2)
    return F 

def init_other(X,V,xstar):
    """ Brief: Non-uniform in x, non-gaussian in v distribution """
    F =  gaussian(V)*V**4*(1+1*np.cos(2 * np.pi * X / xstar))
    return F

def Equilibrium(F,MM,DX,DV,xstar):
    """ Brief: Computes the equilibrium
    
    Input:
    - F:     transient state (meshgrid form)
    - MM:    Maxwellian      (meshgrid form)
    - DX,DV: Cell length     (meshgrid form)
    - xstar: length of the space dommain
    
    Output:
    - mf: Mass
    - Finf: Steady state in meshgrid form
    """
    
    mf = np.sum(F * DX * DV) / xstar
    Finf = MM * mf
    return (Finf,mf)
    
def initAll(F, Finf, MM, DV, mf):
    """ Brief: Computes all useful macroscopic and microscopic quantities from F and Finf
    
     Input: 
     - F:       transient state (meshgrid form)
     - Finf:    Steady state    (meshgrid form)
     - MM:      Maxwelllian     (meshgrid form)
     - DV:      Cell length     (meshgrid form)
     - mf:      Mass
     - epsilon: Knudsen number
    
     Output: 
     - Rho: macroscopic density (meshgrid form)
     - G:   microscopic unknown (meshgrid form)
    """

    Nv = F.shape[0]
    Nx = F.shape[1]
    rho = np.transpose(np.sum(F * DV,0))
    Rho = np.tile(rho,(Nv,1))
    G = F - Rho*MM
    return (Rho,G)

def CFL(dx,m2,vstar,eps,C):
    dtLim = 2*C*dx*dx/m2
    dtFV  = C*min(eps*dx/vstar,eps*eps)
    dt    = min(dtLim, dtFV)
    return (dt,dtLim,dtFV)

# ------------------------------------------------------------- #
                  ##### POST TREATMENT #####
# ------------------------------------------------------------- #
def diagnostic(F, rho, fluid, MM, DV, DX, mf):
    """ Brief: Compute control variates """
    
    L2f = np.sum( (F-mf*MM) * (F-mf*MM)/MM * DV * DX)
    L2Rho = np.sum( (rho-mf) * (rho-mf) * DX[0,:])
    L2fluid = np.sum( (fluid-mf) * (fluid-mf) * DX[0,:])
    return (np.sqrt(L2f), np.sqrt(L2Rho), np.sqrt(L2fluid))

def Conv_analysis(epsilon):
    T = 1.0
    MM9    = np.load('./Order/VlasovBGK_MMCentered_Nx'+str(9)  +'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
    MM27   = np.load('./Order/VlasovBGK_MMCentered_Nx'+str(27) +'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
    MM81   = np.load('./Order/VlasovBGK_MMCentered_Nx'+str(81) +'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
    MM243  = np.load('./Order/VlasovBGK_MMCentered_Nx'+str(243)+'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
    MM729  = np.load('./Order/VlasovBGK_MMCentered_Nx'+str(729)+'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
    MM2187 = np.load('./Order/VlasovBGK_MMCentered_Nx'+str(2187)+'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')

    FV9    = np.load('./Order/VlasovBGK_FV_Nx'+str(9)  +'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
    FV27   = np.load('./Order/VlasovBGK_FV_Nx'+str(27) +'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
    FV81   = np.load('./Order/VlasovBGK_FV_Nx'+str(81) +'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
    FV243  = np.load('./Order/VlasovBGK_FV_Nx'+str(243)+'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
    FV729  = np.load('./Order/VlasovBGK_FV_Nx'+str(729)+'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
    
    Nv = MM9.shape[0]
    vstar = 8
    xstar = np.pi

    (v,a,dv,b_,c,d)        = discretizeV(Nv//2,vstar)

    (x9,a,dx9,b,c,d,e)    = discretizeX(9,xstar)
    (a,b,DV9,c,DX9,d,e)   = discretizeXV(x9,9,dx9,b,v,b_,dv)

    (x27,a,dx27,b,c,d,e)  = discretizeX(27,xstar)
    (a,b,DV27,c,DX27,d,e) = discretizeXV(x27,27,dx27,b,v,b_,dv)

    (x81,a,dx81,b,c,d,e)  = discretizeX(81,xstar)
    (a,b,DV81,c,DX81,d,e) = discretizeXV(x81,81,dx81,b,v,b_,dv)

    (x243,a,dx243,b,c,d,e)  = discretizeX(243,xstar)
    (a,b,DV243,c,DX243,d,e) = discretizeXV(x243,243,dx243,b,v,b_,dv)
    
    (x729,a,dx729,b,c,d,e)  = discretizeX(729,xstar)
    (a,b,DV729,c,DX729,d,e) = discretizeXV(x729,729,dx729,b,v,b_,dv)

    MM9_27   = np.sqrt(np.sum((MM9-MM27[:,::3])**2*DV9*DX9))
    FV9_27   = np.sqrt(np.sum((FV9-FV27[:,::3])**2*DV9*DX9))
    MMFV_9   = np.sqrt(np.sum((MM9-FV9)**2*DV9*DX9))

    MM27_81  = np.sqrt(np.sum((MM27-MM81[:,::3])**2*DV27*DX27))
    FV27_81  = np.sqrt(np.sum((FV27-FV81[:,::3])**2*DV27*DX27))
    MMFV_27  = np.sqrt(np.sum((MM27-FV27)**2*DV27*DX27))

    MM81_243 = np.sqrt(np.sum((MM81-MM243[:,::3])**2*DV81*DX81))
    FV81_243 = np.sqrt(np.sum((FV81-FV243[:,::3])**2*DV81*DX81))
    MMFV_81  = np.sqrt(np.sum((MM81-FV81)**2*DV81*DX81))

    MM243_729 = np.sqrt(np.sum((MM243-MM729[:,::3])**2*DV243*DX243))
    FV243_729 = np.sqrt(np.sum((FV243-FV729[:,::3])**2*DV243*DX243))
    MMFV_243  = np.sqrt(np.sum((MM243-FV243)**2*DV243*DX243))
    
    MM729_2187 = np.sqrt(np.sum((MM729-MM2187[:,::3])**2*DV729*DX729))
    
    OrderMM9_27_27_81 = (np.log(MM9_27)-np.log(MM27_81))/(np.log(DX9[0,0])-np.log(DX27[0,0]))
    OrderFV9_27_27_81 = (np.log(FV9_27)-np.log(FV27_81))/(np.log(DX9[0,0])-np.log(DX27[0,0]))

    OrderMM27_81_81_243 = (np.log(MM27_81)-np.log(MM81_243))/(np.log(DX27[0,0])-np.log(DX81[0,0]))
    OrderFV27_81_81_243 = (np.log(FV27_81)-np.log(FV81_243))/(np.log(DX27[0,0])-np.log(DX81[0,0]))

    OrderMM81_243_243_729 = (np.log(MM243_729)-np.log(MM81_243))/(np.log(DX243[0,0])-np.log(DX81[0,0]))
    OrderFV81_243_243_729 = (np.log(FV243_729)-np.log(FV81_243))/(np.log(DX243[0,0])-np.log(DX81[0,0]))
    
    OrderMM243_729_729_2187 = (np.log(MM729_2187)-np.log(MM243_729))/(np.log(DX729[0,0])-np.log(DX243[0,0]))

    print('=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=')
    print('| Error MMCentered :')
    print('| 9/27     :',"{:.3e}".format(MM9_27))
    print('| 27/81    :',"{:.3e}".format(MM27_81))
    print('| 81/243   :',"{:.3e}".format(MM81_243))
    print('| 243/729  :',"{:.3e}".format(MM243_729))
    print('| 729/2187 :',"{:.3e}".format(MM729_2187))
    print('=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=')
    print('| Error FV :')
    print('| 9/27     :',"{:.3e}".format(FV9_27))
    print('| 27/81    :',"{:.3e}".format(FV27_81))
    print('| 81/243   :',"{:.3e}".format(FV81_243))
    print('| 243/729  :',"{:.3e}".format(FV243_729))
    print('=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=')
    print('| Error FV/MMCentered :')
    print('| 9   :',"{:.3e}".format(MMFV_9))
    print('| 27  :',"{:.3e}".format(MMFV_27))
    print('| 81  :',"{:.3e}".format(MMFV_81))
    print('=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=')
    print('| Ordre MM9_27_27_81', "{:.3e}".format(OrderMM9_27_27_81))
    print('| Ordre FV9_27_27_81', "{:.3e}".format(OrderFV9_27_27_81))
    print('| --------')
    print('| Ordre MM27_81_81_243', "{:.3e}".format(OrderMM27_81_81_243))
    print('| Ordre FV27_81_81_243', "{:.3e}".format(OrderFV27_81_81_243))
    print('| --------')
    print('| OrderMM81_243_243_729', "{:.3e}".format(OrderMM81_243_243_729))
    print('| OrderFV81_243_243_729', "{:.3e}".format(OrderFV81_243_243_729))
    print('| --------')
    print('| OrderMM243_729_729_2187', "{:.3e}".format(OrderMM243_729_729_2187))
    print('=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=')
    
def Conv_analysisHybrid(epsilon, T):
    MM9    = np.load('./Order_H/VlasovBGK_MM_Nx'+str(9)  +'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
    MM27   = np.load('./Order_H/VlasovBGK_MM_Nx'+str(27) +'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
    MM81   = np.load('./Order_H/VlasovBGK_MM_Nx'+str(81) +'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
    MM243  = np.load('./Order_H/VlasovBGK_MM_Nx'+str(243)+'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
    MM729  = np.load('./Order_H/VlasovBGK_MM_Nx'+str(729)+'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
#     MM2187 = np.load('./Order_H/VlasovBGK_MM_Nx'+str(2187)+'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
    
    H_MM9    = np.load('./Order_H/VlasovBGK_H_MM_Nx'+str(9)  +'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
    H_MM27   = np.load('./Order_H/VlasovBGK_H_MM_Nx'+str(27) +'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
    H_MM81   = np.load('./Order_H/VlasovBGK_H_MM_Nx'+str(81) +'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
    H_MM243  = np.load('./Order_H/VlasovBGK_H_MM_Nx'+str(243)+'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
    H_MM729  = np.load('./Order_H/VlasovBGK_H_MM_Nx'+str(729)+'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
#     H_MM2187 = np.load('./Order_H/VlasovBGK_H_MMCentered_Nx'+str(2187)+'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')

#     FV9    = np.load('./Order_H/VlasovBGK_FV_Nx'+str(9)  +'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
#     FV27   = np.load('./Order_H/VlasovBGK_FV_Nx'+str(27) +'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
#     FV81   = np.load('./Order_H/VlasovBGK_FV_Nx'+str(81) +'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
#     FV243  = np.load('./Order_H/VlasovBGK_FV_Nx'+str(243)+'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
#     FV729  = np.load('./Order_H/VlasovBGK_FV_Nx'+str(729)+'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
    
#     H_FV9    = np.load('./Order_H/VlasovBGK_H_FV_Nx'+str(9)  +'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
#     H_FV27   = np.load('./Order_H/VlasovBGK_H_FV_Nx'+str(27) +'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
#     H_FV81   = np.load('./Order_H/VlasovBGK_H_FV_Nx'+str(81) +'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
#     H_FV243  = np.load('./Order_H/VlasovBGK_H_FV_Nx'+str(243)+'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
#     H_FV729  = np.load('./Order_H/VlasovBGK_H_FV_Nx'+str(729)+'L'+str(32)+'T'+str(T)+'eps'+"{:.1e}".format(epsilon)+'.npy')
    
    Nv = MM9.shape[0]
    vstar = 8
    xstar = np.pi

    (v,a,dv,b_,c,d)        = discretizeV(Nv//2,vstar)

    (x9,a,dx9,b,c,d,e)    = discretizeX(9,xstar)
    (a,b,DV9,c,DX9,d,e)   = discretizeXV(x9,9,dx9,b,v,b_,dv)

    (x27,a,dx27,b,c,d,e)  = discretizeX(27,xstar)
    (a,b,DV27,c,DX27,d,e) = discretizeXV(x27,27,dx27,b,v,b_,dv)

    (x81,a,dx81,b,c,d,e)  = discretizeX(81,xstar)
    (a,b,DV81,c,DX81,d,e) = discretizeXV(x81,81,dx81,b,v,b_,dv)

    (x243,a,dx243,b,c,d,e)  = discretizeX(243,xstar)
    (a,b,DV243,c,DX243,d,e) = discretizeXV(x243,243,dx243,b,v,b_,dv)
    
    (x729,a,dx729,b,c,d,e)  = discretizeX(729,xstar)
    (a,b,DV729,c,DX729,d,e) = discretizeXV(x729,729,dx729,b,v,b_,dv)
    
    (x2187,a,dx2187,b,c,d,e)  = discretizeX(2187,xstar)
    (a,b,DV2187,c,DX2187,d,e) = discretizeXV(x2187,2187,dx2187,b,v,b_,dv)

    MM9    = np.sqrt(np.sum((MM9   - H_MM9)**2*DV9*DX9))
    MM27   = np.sqrt(np.sum((MM27  - H_MM27)**2*DV27*DX27))
    MM81   = np.sqrt(np.sum((MM81  - H_MM81)**2*DV81*DX81))
    MM243  = np.sqrt(np.sum((MM243 - H_MM243)**2*DV243*DX243))
    MM729  = np.sqrt(np.sum((MM729 - H_MM729)**2*DV729*DX729))
#     MM2187 = np.sqrt(np.sum((MM2187- H_MM2187)**2*DV2187*DX2187))
    
#     FV9    = np.sqrt(np.sum((FV9   - H_FV9)**2*DV9*DX9))
#     FV27   = np.sqrt(np.sum((FV27  - H_FV27)**2*DV27*DX27))
#     FV81   = np.sqrt(np.sum((FV81  - H_FV81)**2*DV81*DX81))
#     FV243  = np.sqrt(np.sum((FV243 - H_FV243)**2*DV243*DX243))
#     FV729  = np.sqrt(np.sum((FV729 - H_FV729)**2*DV729*DX729))
#     FV2187 = np.sqrt(np.sum((FV2187- H_FV2187)**2*DV2187*DX2187))

    OrderMM9_27 = (np.log(MM9)-np.log(MM27))/(np.log(DX9[0,0])-np.log(DX27[0,0]))
#     OrderFV9_27 = (np.log(FV9)-np.log(FV27))/(np.log(DX9[0,0])-np.log(DX27[0,0]))

    OrderMM27_81 = (np.log(MM27)-np.log(MM81))/(np.log(DX27[0,0])-np.log(DX81[0,0]))
#     OrderFV27_81 = (np.log(FV27)-np.log(FV81))/(np.log(DX27[0,0])-np.log(DX81[0,0]))

    OrderMM81_243 = (np.log(MM243)-np.log(MM81))/(np.log(DX243[0,0])-np.log(DX81[0,0]))
#     OrderFV81_243 = (np.log(FV243)-np.log(FV81))/(np.log(DX243[0,0])-np.log(DX81[0,0]))
    
    OrderMM243_729 = (np.log(MM729)-np.log(MM243))/(np.log(DX729[0,0])-np.log(DX243[0,0]))
#     OrderFV243_729 = (np.log(FV729)-np.log(FV243))/(np.log(DX729[0,0])-np.log(DX243[0,0]))
    
#     OrderMM729_2187 = (np.log(MM2187)-np.log(MM729))/(np.log(DX2187[0,0])-np.log(DX729[0,0]))

    print('=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=')
    print('| Error MMCentered :')
    print('| 9    :',"{:.3e}".format(MM9))
    print('| 27   :',"{:.3e}".format(MM27))
    print('| 81   :',"{:.3e}".format(MM81))
    print('| 243  :',"{:.3e}".format(MM243))
    print('| 729  :',"{:.3e}".format(MM729))
#     print('| 2187 :',"{:.3e}".format(MM2187))
#     print('=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=')
#     print('| Error FV :')
#     print('| 9    :',"{:.3e}".format(FV9))
#     print('| 27   :',"{:.3e}".format(FV27))
#     print('| 81   :',"{:.3e}".format(FV81))
#     print('| 243  :',"{:.3e}".format(FV243))
#     print('| 729  :',"{:.3e}".format(FV729))
    print('=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=')
    print('| Ordre MM9_27', "{:.3e}".format(OrderMM9_27))
#     print('| Ordre FV9_27', "{:.3e}".format(OrderFV9_27))
    print('| --------')
    print('| Ordre MM27_81', "{:.3e}".format(OrderMM27_81))
#     print('| Ordre FV27_81', "{:.3e}".format(OrderFV27_81))
    print('| --------')
    print('| OrderMM81_243', "{:.3e}".format(OrderMM81_243))
#     print('| OrderFV81_243', "{:.3e}".format(OrderFV81_243))
    print('| --------')
    print('| OrderMM243_729', "{:.3e}".format(OrderMM243_729))
#     print('| OrderFV243_729', "{:.3e}".format(OrderMM243_729))
    print('| --------')
#     print('| OrderMM729_2187', "{:.3e}".format(OrderMM243_729))
    print('=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=')